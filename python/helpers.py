
# Various tidy-ish helper classes that are better of here than in the 
# middle of to-be-messy bot AI or cluttering game data models

import math, numpy

class Struct:
	"""
	generic Structure for converting JSON-originated dictionaries
	to Python objects that work like in JavaScript (see unittests.py)
	"""
	def __init__(self, dictionary):
		self._update( dictionary )
	
	def _update( self, other ):
		if isinstance( other, dict ): dictionary = other
		elif isinstance( other, Struct ): dictionary = other.__dict__
		for key, value in dictionary.iteritems():
			if isinstance( value, dict ): assignedValue = Struct( value )
			elif isinstance( value, list ):
				assignedValue = [ Struct(v) if isinstance(v, dict) else v for v in value ]
			else: assignedValue =  value
			self.__dict__[key] = assignedValue
	
	def __str__( self ):
		parts = []
		for key,value in self.__dict__.iteritems():
			parts.append(str(key)+': '+str(value))
		return str(self.__class__) + '(' + ', '.join(parts) + ')'


# Mathemagics

class LinearResponse:
	# uncommented code with three-dimensional matrices
	
	def __init__(self, transition, columnNames, n ):
		
		state = numpy.eye( *(transition.shape) )
		self.transitionMatrix = transition
		self.data = numpy.zeros( transition.shape + (n,) )
		
		for t in xrange(n):
			state = numpy.dot( transition, state )
			self.data[:,:,t] = state
			
		self.columns = {columnNames[i]: i for i in range(len(columnNames))}
		
		for name in self.columns.keys():
			setattr( self, name, Struct({
				'delayed': {
						'responseTo':
						{ toWhat : self.delayedShift(
							self.responseFunction( name, toWhat ))
							for toWhat in self.columns.keys() }
					},
				'responseTo':
					{ toWhat : self.responseFunction( name, toWhat )
						for toWhat in self.columns.keys() },
				'responseToState':
					lambda state,name=name:
						getattr( self.responseToState( state ), name ),
				'integrateResponseTo':
					{ toWhat :
						(lambda seq,ofWhat=name,toWhat=toWhat:
							self.integrateResponse( ofWhat, toWhat, seq ))
						for toWhat in self.columns.keys() }
			}) )
	
	def responseFunction( self, ofWhat, toWhat ):
		ofColumn = self.columns[ofWhat]
		toColumn = self.columns[toWhat]
		return self.data[ofColumn,toColumn,:]
	
	def delayedShift( self, what ):
		d = numpy.roll(what, 1)
		d[0] = 0.0
		return d
		
	def parseState( self, state ):
		# ensure column vector
		stateArr = numpy.zeros( (len(self.columns), 1) )
		
		for column,index in self.columns.iteritems():
			if isinstance( state, Struct ):
				stateArr[index] = [ getattr(state, column)  ]
			elif isinstance( state, dict ):
				stateArr[index] = state[column]
			else:
				stateArr[index] = state[index]
		return stateArr
		
	
	def responseToState( self, state ):
		
		state = self.parseState( state )
		
		mat = numpy.tensordot( self.data, state, axes=(1,0) )[:,:,0]
		
		result = {}
		for name,idx in self.columns.iteritems():
			result[name] = mat[idx,:]
		
		return Struct( result )
	
	def integrateResponse( self, ofWhat, toWhat, drivingSeq ):
		
		responseFunction = self.responseFunction( ofWhat, toWhat )
		response = numpy.convolve( drivingSeq, responseFunction )
		return response[:self.data.shape[-1]]

class Vector2:
	"""helper (for visualization mostly): 2D-vector"""
	
	def __init__(self, x,y):
		self.x = x
		self.y = y
	
	@staticmethod
	def fromAngle(angle):
		rads = (-angle + 90) / 180.0 * math.pi
		return Vector2( math.cos(rads), math.sin(rads) )
	
	def perpCw(self):
		"""
		return a new vector obtained by rotating this one 90
		degrees clockwise
		"""
		return Vector2( self.y, -self.x )
	
	def __add__(self, other):
		return Vector2(self.x + other.x, self.y + other.y)
	
	def __mul__(self, scalar):
		return Vector2(self.x * scalar, self.y * scalar)
