
# Learning physics

import numpy

class Physics:
	"""Game physics parameters."""
	
	GUESS_CACC_THRESHOLD = 0.2 #0.32
	GUESS_CACC_MULT = 3.2
	GUESS_ANGLE_SPRING = 0.01
	GUESS_ANGLE_DAMPING = 0.01
	
	# Numerical error tolerance
	TOL = 1e-9
	
	def guessCurveTorqueFunction( self, curvature, velocity, safetyMult = 1.0 ):
		"""
		Models the angular acceleration of the car as a function of
		centripetal acceleration in a bend. This version is used
		as a guess if relevant parameters are unknown
		"""
		
		cacc = velocity**2 * curvature * safetyMult
		
		thr = Physics.GUESS_CACC_THRESHOLD
		belowThreshold = numpy.abs(cacc) < thr
		cacc -= thr * numpy.sign(cacc)
		if belowThreshold: cacc = 0.0
		cacc *= Physics.GUESS_CACC_MULT
		
		return cacc
	
	def resolvedTorqueFunction( self, slope, curvature, velocity, safetyMult = 1.0 ):
		"""
		Models the angular acceleration of the car as a function of
		centripetal acceleration in a specific bend of the track
		"""
		
		cacc = velocity**2 * curvature * safetyMult
		torque = slope * cacc - self.friction_coeff_v * velocity * numpy.sign(cacc)
		
		if torque * numpy.sign(cacc) <= 0: torque = 0.0
		return torque
	
	def laneTorqueFunction( self, lane, velocity, safetyMult = 1.0 ):
		curv = lane.signedCurvature
		meas = self.laneTorqueMeasurements.get( lane, None )
		if meas != None and meas.slope != None:
			return self.resolvedTorqueFunction( meas.slope, curv, velocity, safetyMult )
		elif abs(curv) in self.curvatureMap:
			return self.resolvedTorqueFunction( self.curvatureMap[abs(curv)], curv, velocity, safetyMult )
		else:
			return self.guessCurveTorqueFunction( lane.signedCurvature, velocity, safetyMult )
	
	def inverseThrottle( self, velocity, turboFactor = 1.0 ):
		"""Throttle required to maintain a given velocity"""
		
		# stored these in less nice format, not changing, could
		# break things...
		alpha = 1.0 / (1.0 - self.velocity_coefficient)
		beta = self.throttle_coefficient * alpha * turboFactor
		
		th = (1.0 - alpha) / beta * velocity
		
		# clamping does not give the correct answer but at least
		# yields a valid throttle value
		return min( max( th, 0.0 ), 1.0 )
	
	def specialTick( self, race ):
		myCar = race.myCar
		if not myCar.isOnTrack(): return True
		if race.gameTick < 3: return True
		if myCar.derivatives.justInitialized: return True
		return False
	
	def onMyCrash( self ):
		ang = self.lastAngle
		
		print "crashed after angle", ang,
		
		self.max_angle = max(self.maxObservedAngle, 10.0)
		print "setting max angle to observed angle", self.max_angle
	
	class RollingBuffer:
		def __init__(self, windowLen = 10):
			self.buf = None
			self.windowLen = windowLen
		
		@property
		def name( self ): return self.__class__.__name__
		
		def linSolve( self, x, y, nofitValue = None ):
			
			if x.shape[0] == 0 or x.shape[0] <= x.shape[1] + 1:
				#print 'not enough suitable data in', self.name
				return None
			
			res = numpy.linalg.lstsq( x, y )
			
			err = sum(res[1]**2)
			if err > Physics.TOL:
				print "ouch!", self.name, "does not fit :(", err
				return nofitValue
			elif res[2] < len(res[0]):
				print 'low-rank data in', self.name
				return None
			
			return res[0]
		
		def pushRow(self, newRow):
			"""grow or rotate a measurement buffer"""
			
			if self.buf == None:
				self.buf = numpy.zeros((0,len(newRow)))
			
			self.buf = numpy.vstack( (self.buf, newRow) )
			if self.buf.shape[0] > self.windowLen:
				self.buf = numpy.delete( self.buf, 0, 0 )
			else:
				#print 'growing', self.name, '...'
				pass
				
			return self.buf
	
	class LinearMotionBuffer(RollingBuffer):
		
		def update( self, race ):
			myCar = race.myCar
			
			row = [ myCar.throttle * myCar.turboFactor,
					myCar.derivatives.velocity,
					myCar.derivatives.forwardAcceleration ]
				
			self.pushRow( row )
		
		def resolve( self, physics ):
			
			if sum( self.buf[:,0] != 0 ) < 2:
				print "not enough throttle, not resolving", self.name
			
			sol = self.linSolve( self.buf[:,:2], self.buf[:,2] )
			if sol != None:
				a,b = sol
				if abs(b) < Physics.TOL:
					print "zero throttle coeff. I refuse to believe it"
				else:
					physics.throttle_coefficient = a
					physics.velocity_coefficient = b
					physics.resolvedLinear = True
					return True
			return False
	
	class AngularMotionBuffer(RollingBuffer):
		
		def update( self, race ):
			myCar = race.myCar
			
			angvel = myCar.derivatives.angularVelocity
			ang = myCar.position.angle - angvel
			vel = myCar.derivatives.velocity
			
			row = [ race.gameTick,
					angvel,
					ang * vel,
					angvel * vel,
					vel,
					myCar.position.lane.signedCurvature,
					myCar.derivatives.angularAcceleration ]
			
			self.pushRow( row )
		
		def getValidShifted( self ):
			
			ticks = self.buf[:,0]
			consec = numpy.zeros(ticks.shape, dtype=bool)
			shiftedAngAcc = numpy.zeros(ticks.shape)
			
			consec[:-1] = numpy.diff( ticks ) == 1
			shiftedAngAcc[:-1] = self.buf[1:,-1]
			
			return (shiftedAngAcc[consec], self.buf[consec,:])
		
		def resolve( self, physics ):
			
			angacc, params = self.getValidShifted()
			free = params[:,-2] == 0.0
			if angacc.size == 0 or free.size == 0: return False
			angacc = angacc[free]
			params = params[free, 1:4]
			
			sol = self.linSolve( params, angacc )
			if sol == None: return False
			
			cur = numpy.array([physics.angle_osc_angvel,
							   physics.angle_osc_angle_v,
							   physics.angle_osc_angvel_v])
								
			diff = numpy.sum((cur-sol)**2)
								
			if diff > Physics.TOL:
				print "angle params changed", str(sol)
				physics.angle_osc_angvel, physics.angle_osc_angle_v, physics.angle_osc_angvel_v = sol
				
			return True
	
	class FrictionBuffer(RollingBuffer):
		
		def resolve( self, physics ):
			# pick most common (mode)
			frictions,inverseIndex = numpy.unique(self.buf[:,0].round(decimals=7), return_inverse=True)
			#print frictions, inverseIndex
			
			mostCommonIndex = numpy.argmax(numpy.bincount(inverseIndex))
			friction = self.buf[inverseIndex[mostCommonIndex],0]
			
			if abs(physics.friction_coeff_v - friction) > Physics.TOL:
				physics.resetFriction( friction )
				
				print frictions, inverseIndex, numpy.bincount(inverseIndex), self.buf[:,0], mostCommonIndex, inverseIndex[mostCommonIndex]
				print "friction coeff changed"
				return True
			
			return False
	
	class TorqueBuffer(RollingBuffer):
		
		def __init__( self, lane, windowLen = 15 ):
			Physics.RollingBuffer.__init__(self, windowLen)
			self.lane = lane
			self.curvature = lane.signedCurvature
			self.slope = None
			self.anomalyCounter = 0
		
		@property
		def name(self): return 'TorqueBuffer for '+str(self.lane)
		
		def update( self, race, angularBuffer, physics ):
			if round(angularBuffer.buf[-1,0]) == round(race.gameTick):
				
				angacc, params = angularBuffer.getValidShifted()
				
				if len(angacc) == 0: return False
				
				angRow = params[-1,:]
				angacc = angacc[-1]
				gameTick = int(angRow[0])
				
				if round(gameTick) == round(race.gameTick)-1 and \
						abs(angRow[-2] - self.curvature) < Physics.TOL:
					
					freeAcc = angRow[1] * physics.angle_osc_angvel \
						+ angRow[2] * physics.angle_osc_angle_v \
						+ angRow[3] * physics.angle_osc_angvel_v
						
					torque = angacc - freeAcc
					vel = angRow[4]
					cacc = vel**2 * self.curvature
					
					self.pushRow( [gameTick, cacc, vel, torque] )
					return True
				
			return False
		
		def resolve( self, physics ):
			
			#print 'torque buf:', self.buf
			
			torque = self.buf[:,-1]
			x = self.buf[:,1:-1]
			
			nonzeros = abs(torque) > Physics.TOL
			
			#print 'resolving...', torque
			
			sol = self.linSolve( x[nonzeros,:], torque[nonzeros], 'nofit' )
			if sol == None:
				#print 'bad torque buf:', self.buf
				return False
			elif sol == 'nofit':
				self.anomalyCounter += 1
				physics.pieceAnomalyCounters[self.lane.piece] = \
					physics.pieceAnomalyCounters.get(self.lane.piece,0)+1 
				self.slope = None
				return False
			
			slope, friction = sol
			friction = abs(friction)
			slope = abs(slope)
			#print self.curvature, sol
			
			if self.anomalyCounter > 0: self.anomalyCounter -= 1
			
			if physics.pieceAnomalyCounters.get(self.lane.piece,0) > 0:
				physics.pieceAnomalyCounters[self.lane.piece] -= 1
					
			#print 'resolved, anomalyCounter', self.anomalyCounter
					
			if self.slope == None or abs(self.slope - slope) > Physics.TOL:
				
				if abs(slope) > 20.0:
					wasAnomaly = self.anomalyCounter > 0
					self.anomalyCounter += 2
					self.slope = None
					print "warning: rejecting slope", slope
					return not wasAnomaly
				
				print "resolved", self.name, slope 
				
				if self.anomalyCounter > 0:
					print 'skipping update due to previous anomalies'
				else:
					self.slope = slope
					
					physics.frictionMeasurements.pushRow( [friction] )
					if physics.frictionMeasurements.resolve( physics ): return True
					
					absCurv = abs(self.curvature)
					oldCurvSlope = physics.curvatureMap.get( absCurv, 0.0 )
					if abs(oldCurvSlope - slope) > Physics.TOL:
						print "changed slope for curvature", absCurv
						physics.curvatureMap[absCurv] = slope
					
					return True
			
			return False
			
	def update( self, race ):
		
		myCar = race.myCar
		TOL = Physics.TOL
		
		# skip special cases (e.g., me crashed)
		if self.specialTick( race ): return False
		
		# --- linear motion
		
		# store linear motion data
		self.linearMeasurements.update( race )
		
		# learn from crashes ... TODO
		self.lastAngle = myCar.position.angle
		
		# skip learning if other cars on the same or adjacent lanes
		blocked = race.getBlockedLanes(False,True)
		if myCar.position.lane in blocked:
			print "other cars on lane"
			return False
		
		for car in race.cars.values():
			if car.isOnTrack():
				self.maxObservedAngle = max(self.maxObservedAngle, abs(car.position.angle))
		
		if self.maxObservedAngle > self.max_angle:
			print "angle greater than", self.max_angle, "observed, changing max_angle"
			self.max_angle = self.maxObservedAngle
		
		# learn linear motion coefficients
		if not self.resolvedLinear:
			self.lastAnomaly = True
			if self.linearMeasurements.resolve( self ):
				self.resolvedLinear = True
				return True
			return False
		
		predAcc = myCar.throttle * myCar.turboFactor * self.throttle_coefficient
		predAcc += myCar.derivatives.velocity * self.velocity_coefficient
		
		err = myCar.derivatives.forwardAcceleration - predAcc
		
		# learn about switches
		if myCar.derivatives.laneChange:
			print 'lane change'
		elif myCar.derivatives.pieceChange:
			print 'piece change'
		
		if myCar.derivatives.pieceChange:
			fromLane = myCar.derivatives.prevPrevLane
			toLane = myCar.derivatives.prevLane
			if fromLane != None and toLane != None and fromLane.index != toLane.index:
				
				if toLane not in fromLane.switchLengths:
					fromLane.switchLengths[toLane] = 0.0
				fromLane.switchLengths[toLane] += err
				
				if abs(err) > TOL:
					print 'adjusting gap',err,'to switch',
					print fromLane.index, '->', toLane.index, 'at p.',
					print fromLane.piece.index
					
					myCar.derivatives.velocity -= err
					myCar.derivatives.forwardAcceleration -= err
					
					self.lastAnomaly = True
					return True
				else:
					print 'safe switch'
		
		if abs(err) > TOL:
			print "acceleration anomaly", err
			return False
		
		if self.lastAnomaly:
			self.lastAnomaly = False
			return False
		
		if myCar.turboFactor != 1.0: return False
		
		
		# --- angular motion
		
		# (should reach these lines only if there was nothing special
		#  going on with linear motion)
		
		if abs(myCar.derivatives.angularVelocity) == 0.0:
			# no angular activity, not trying to learn anything
			return False
			
		self.angleMeasurements.update( race )
		
		if self.angleMeasurements.resolve( self ):
			if not self.resolvedAngular:
				self.resolvedAngular = True
				print "resolved free oscillation parameters"
				return True
		
		if not self.resolvedAngular: return False
		
		self.measuredPieces[myCar.position.piece] = True
		
		# learn about torque and friction
		
		lane = myCar.position.lane
		curv = lane.signedCurvature
		if curv == 0.0: return False
		
		meas = self.laneTorqueMeasurements.get( lane, None )
		if meas == None: meas = Physics.TorqueBuffer(lane)
		self.laneTorqueMeasurements[lane] = meas
		
		if meas.update( race, self.angleMeasurements, self ):
			if meas.resolve( self ): return True
		
		return False
	
	def resetLaneTorque( self, lane ):
		print 'lane torque reset'
		meas = self.laneTorqueMeasurements.get(lane,None)
		if meas != None:
			if meas.slope != None:
				#meas = Physics.TorqueBuffer(lane)
				#self.laneTorqueMeasurements[lane] = meas
				meas.anomalyCounter += 1
				meas.slope = None
		else:
			self.curvatureMap.pop(abs(lane.signedCurvature), None)
	
	def resetFriction( self, friction ):
		
		self.nFrictionResets += 1
		if self.nFrictionResets > 5:
			self.nFrictionResets = 0
			print "warning: multiple friction resets. doing full reset"
			self.fullReset()
			self.frictionAnomaly = True
		elif self.nFrictionResets > 1 and (friction < 0.05 or friction > 0.7):
			self.nFrictionResets = 0
			print "warning: non-plausible friction coefficient", friction, 'doing full reset'
			self.fullReset()
			self.frictionAnomaly = True
		else:
			self.friction_coeff_v = friction
			self.laneTorqueMeasurements = {}
			self.curvatureMap = {}
			self.measuredPieces = {}
			self.pieceAnomalyCounters = {}
			self.frictionAnomaly = False
	
	def fullReset( self ):
		# guesses
		
		self.velocity_coefficient = -1/50.0
		self.throttle_coefficient = 10.0 / 50.0
		
		self.max_angle = 60.0
		
		self.angle_osc_angvel = - 0.1
		self.angle_osc_angle_v = -0.00125
		self.angle_osc_angvel_v = -0.00125
		
		self.nFrictionResets = 0
		self.resetFriction( 0.3 )
		
		# learning control info
		
		self.resolvedLinear = False
		self.resolvedAngular = False
		
		self.lastAnomaly = True
		
		self.angleMeasurements = Physics.AngularMotionBuffer(30)
		self.linearMeasurements = Physics.LinearMotionBuffer()
		self.frictionMeasurements = Physics.FrictionBuffer(30)
		
		self.maxObservedAngle = 0.0
	
	def __str__( self ):
		
		doFormat = lambda x: "\n\t"+x+': '+str(getattr(self, x))
		
		s = doFormat('velocity_coefficient')[1:]
		s += doFormat('throttle_coefficient')
		s += doFormat('max_angle')
		s += doFormat('angle_osc_angvel')
		s += doFormat('angle_osc_angle_v')
		s += doFormat('angle_osc_angvel_v')
		s += doFormat('friction_coeff_v')
		s += doFormat('maxObservedAngle')
		
		# see comment in inverseThrottle
		alpha = 1.0 / (1.0 - self.velocity_coefficient)
		beta = self.throttle_coefficient * alpha
		vmax = beta / (1.0-alpha)
		
		s += '\n\t -> maximum velocity: %.5g' % vmax
		
		return s
	
	def __init__( self ):
		
		self.fullReset()
		print "starting with physics:\n"+str(self)
		
