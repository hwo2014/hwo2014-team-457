import json, socket, sys, os, time

import bot
import models
import statistics

TIMING = os.environ.get('___TIMING') != None
VISUALIZATION = os.environ.get('___VISUALIZATION') != None
SAVE_STATS = os.environ.get('___SAVE_STATS') != None
DEBUG = os.environ.get('___DEBUG') != None
if VISUALIZATION: from visualization import Visualization
if TIMING: import timeit

class Game:
	"""
	interprets server messages and send appropriate responses.
	contains some logic that is nice to separate from the socket,
	JSON and game loop stuff for unit testing (TODO: could be merged
	with the Race model...)
	"""
	
	def __init__(self, name, key, botClass = bot.COMPETITION_BOT_CLASS ):
			
		self.name = name
		self.key = key
		
		self.race = None
		self.bot = None
		self.stats = None
		self.botClass = botClass
		
		envBotClass = os.environ.get('___BOT_CLASS')
		if envBotClass != None:
			import dummies
			self.botClass = eval('dummies.'+envBotClass.strip())
	
	def onGameInit(self, msg):
		
		if self.race == None:
			self.race = models.Race()
			
			self.bot = self.botClass( self.race, self.bot )
			self.stats = statistics.Statistics( self.race )
				
			self.race.receiveMessage( self.lastYourCar )
			self.race.receiveMessage( msg )
			
			self.bot.onGameInit()
		
		if VISUALIZATION:
			self.visu = Visualization( self.race, self.bot, self.stats )
	
	def onGameTick( self ):
		
		self.stats.assimilate()
		if VISUALIZATION: self.visu.render()
		
		response = self.bot.makeMove()
		action,data = response
		
		# need to feed back some responses to other models
		
		if action == 'throttle':
			self.race.myCar.throttle = data
		
		if action == 'turbo':
			self.race.myCar.activateTurbo( self.race.gameTick )
		
		gameTick = self.race.gameTick
		return (action, data, gameTick)
	
	def joinMessage( self ):
		"""return a join command (sent to the server first)"""
		
		botId = {"name": self.name, "key": self.key}
		
		track = os.environ.get('___TRACK')
		password = os.environ.get('___PASSWORD')
		carCount = os.environ.get('___CAR_COUNT')
		joinInsteadOfCreate = os.environ.get('___JOIN') != None
		
		if track != None:
			msg = { 'botId': botId, 'trackName': track }
			if password != None: msg['password'] = password
			if carCount != None: msg['carCount'] = int(carCount)
			if joinInsteadOfCreate:
				print "sending joinRace: "+str(msg)
				return ('joinRace', msg)
			else:
				print "sending createRace: "+str(msg)
				return ('createRace', msg)
		else:
			print "normal join"
			return ("join", botId)
	
	def receiveMessage( self, msg ):
		"""
		communicates a server message to appropriate models
		returns response, consulting the Bot if required
		"""
		
		msgType = msg['msgType']
		
		# a hack to allow full reinitialization of model objects
		# between qualifying round and race
		if msgType == 'yourCar': self.lastYourCar = msg
		
		response = None
		
		if msgType == 'error': 
			sys.stderr.write("Error: {0}\n".format(msg['data']))
			
		elif msgType == 'gameInit':
			self.onGameInit( msg )
		else:
			if self.race != None: self.race.receiveMessage( msg )
			
			if msgType == 'carPositions':
				if self.race.started:
					response = self.onGameTick()
			else:
				if not DEBUG:
					print('received '+str(json.dumps(msg)))
				if msgType == 'gameStart':
					# should apparently respond to gameStart with a
					# bot command. Always respond with full throttle
					response = ('throttle', 1.0)
		
		# print special sent messages to stdout
		if response == None:
			if DEBUG: print '(not sending response)'
		elif not DEBUG and response[0] not in ['ping','throttle']:
			print "sending "+str(response)
		
		return response

	def afterGame(self):
		"""
		called on program exit (even on iterrupt) useful for 
		gathering statistics from partial games
		"""
		if SAVE_STATS and self.stats != None: self.stats.save()


	
class Controller(object):
	"""
	handles socket stuff, JSON, and runnnig the main loop.
	sends the messages and queries responses from the Game
	object.
	"""

	def __init__(self, host, port, game):
		
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.connect((host, int(port)))
		
		self.game = game

	def run(self):
		self.msg(*self.game.joinMessage())
		try:
			self.msg_loop()
		finally:
			game.afterGame()
		
	def msg(self, msg_type, data = None, gameTick = None):
		if data == None: data = {}
		msg = {"msgType": msg_type, "data": data}
		if gameTick != None: msg['gameTick'] = gameTick
		msg = json.dumps(msg)
		if DEBUG: print 'sending JSON: '+msg
		self.socket.sendall(msg + "\n")
	
	def msg_loop(self):
		socket_file = self.socket.makefile()
		line = socket_file.readline()
		while line:
			if TIMING: t0 = timeit.default_timer()
			if DEBUG: print 'received: '+line
			msg = json.loads(line)
			
			move = self.game.receiveMessage( msg )
			if move != None: self.msg( *move )
			
			if TIMING:
				elapsed = timeit.default_timer() - t0
				print('processed tick in %.2g ms' % (elapsed * 1000))
				
			line = socket_file.readline()

if __name__ == "__main__":
	if len(sys.argv) != 5:
		print("Usage: ./run host port botname botkey")
	else:
		host, port, name, key = sys.argv[1:5]
		
		print("Connecting with parameters:")
		print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
		
		game = Game(name, key)
		controller = Controller(host, port, game)
		controller.run()
