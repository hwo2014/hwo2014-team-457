

import os, sys, time
import dummies, bot

def clearIfSet( envVar ):
	if os.environ.get(envVar) != None: del os.environ[envVar]

host,port,key,nCars = sys.argv[1:]
nCars = int(nCars)

print 'race with', nCars, 'cars'

primaryName = '___'
#opponents = [bot.COMPETITION_BOT_CLASS]
#bots = [bot.LocalControlBot] + dummies.DUMMY_BOT_CLASSES
#opponents = [dummies.MyBotWithoutSwitching]


#bots = [bot.LocalControlBot] + dummies.DUMMY_BOT_CLASSES
bots = [bot.COMPETITION_BOT_CLASS,
       dummies.TrafficJamBot,
       dummies.SafeBot,
       dummies.SlowerMyBot,
       dummies.COMPETITION_BOT_CLASS,
	   dummies.RoutedSafeBot]

#bots = [bot.LocalControlBot, dummies.MyBotWithoutTurbo]

RUN_CMD = 'python main.py'
#RUN_CMD = 'echo'

try:
	os.environ['___CAR_COUNT']=str(nCars)
	os.environ['___BOT_CLASS'] = bots[0].__name__
	os.system('env|grep ___')
	os.system(' '.join([RUN_CMD, host, port, primaryName, key, '&']))
	time.sleep(5.2)
	clearIfSet('___SAVE_STATS')
	clearIfSet('___VISUALIZATION')
	os.environ['___JOIN'] = 'true'
	
	for j in xrange(1,nCars):
		print '--- opponent', j
		opponent = bots[j].__name__
		opponent_name = opponent[:15]+str(j)
		os.environ['___BOT_CLASS'] = opponent
		cmd = ' '.join([RUN_CMD, host, port, opponent_name, key, '> /dev/null'])
		if j < nCars-1: cmd += ' &'
		os.system('env|grep ___')
		os.system(cmd)
		time.sleep(1.7)
finally:
	# this is how I usually manage my processes
	os.system('killall -9 python')
	#pass
