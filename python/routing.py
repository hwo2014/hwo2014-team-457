
# Routing on track

from helpers import Struct

class TrackGraph:
	"""
	Encapsulates things about routing on a track. Contains the track
	graph, whose nodes correspond to Track.Lanes and Edges the allowed
	switches between them (including staying on the same track).
	
	"""
	
	class Edge:
		"""An edge between two nodes (which represent lanes)"""
		
		def __init__( self, targetNode, intDirection ):
			
			self.intDirection = intDirection
			self.directionName = {
				0: None,
				-1: 'Left',
				1: 'Right' }[intDirection]
			
			self.targetNode = targetNode
			self.routingData = {}

	class Route:
		"""
		Route describes a path in the track graph (a valid route on the
		game track). It is guaranteed to reach finish but may not start
		at first track position. It consists of "lane data" points that
		contain global route distances and references to the routing
		graph.
		"""
		
		def __init__(self, routing, routingCriterion, beginLane, prevLane = None ):
		
			self.criterion = routingCriterion
			self.laneDataMap = {}
			self.laneDataList = []
			
			node = routing.laneGraph[beginLane]
			prevNode = None
			
			dist = 0.0
			
			for j in xrange(len(routing.track.pieces)):
				edge = node.routingData[routingCriterion].bestEdge
				nextNode = edge.targetNode
				
				laneData = Struct({
					'lane': node.lane,
					'routingEdge': edge,
					'routingNode': node,
					'pathDistance': dist,
					'pathIndex': j
				})
				self.laneDataMap[node.lane] = laneData
				self.laneDataList.append(laneData)
				
				dist += node.lane.length
				if prevLane != None and node.lane in prevLane.switchLengths:
					dist -= prevLane.switchLengths[node.lane]
				
				if node.lane.piece.finish: self.finishLength = dist
				
				prevNode = node
				prevLane = node.lane
				node = nextNode
				if node.lane in self.laneDataMap: break
			
			self.totalPathDistance = dist
			self.numberOfPathElements = self.laneDataList[-1].pathIndex+1
			
			for laneData in self.laneDataList:
				piecesLeft = self.numberOfPathElements - laneData.pathIndex
				laneData.pathElementsLeft = piecesLeft
				laneData.routeDistanceLeft = self.totalPathDistance - laneData.pathDistance
		
		def signedLaneCurvatures( self ):
			return map( lambda data : data.lane.signedCurvature, laneDataList )
		
		def laneBeginDistances( self ):
			return map( lambda data : data.beginDistance, laneDataList )
		
		def laneSequence( self ):
			return map( lambda data : data.lane.index, laneDataList )
		
		def switchAt( self, lane ):
			return self.laneDataMap[ lane ].routingEdge.directionName
		
		def hasSwitchAt( self, lane ):
			targetLane = self.laneDataMap[ lane ].routingEdge.targetNode.lane
			return targetLane.index != lane.index
			
		def switchLengthAt( self, lane ):
			targetLane = self.laneDataMap[ lane ].routingEdge.targetNode.lane
			if targetLane.index == lane.index: return 0.0
			return lane.switchLengths[targetLane]
		
		def switchResolvedAt( self, lane ):
			targetLane = self.laneDataMap[ lane ].routingEdge.targetNode.lane
			if targetLane.index == lane.index: return True
			return targetLane in lane.switchLengths
		
		def pathStartingFrom( self, lane ):
			beginIndex = self.laneDataMap[lane].pathIndex
			return self.laneDataList[beginIndex:]
			
		def allSwitchesResolved( self, startingFrom, n ):
			prevLane = None
			for elem in self.pathStartingFrom( startingFrom )[:n]:
				lane = elem.lane
				if (prevLane != None and prevLane.index != lane.index and
					lane not in prevLane.switchLengths): return False
				prevLane = lane
			return True
			
		
		def hasLane( self, lane ): return lane in self.laneDataMap
	
	def routeBy( self, criterionName, nodeFunction, edgeFunction, lastPiece = None ):
		"""
		Generate routing table by backpropagating certain
		criteria along the track
		"""
		
		if lastPiece == None:
			lastPiece = self.track.pieces[-1]
		
		nextPiece = None
		piece = lastPiece
		
		# Run loop backwards along the pieces of the track, with
		# one piece overlap at the last piece
		while True:
			
			for lane in piece.lanes:
				node = self.laneGraph[lane]
				
				if nextPiece == None:
					# node data at start
					nodeData = nodeFunction( node, None )
					node.routingData[criterionName] = nodeData
				else:
					edgePriorities = {}
					edgeData = {}
					for edge in node.edges:
						targetData = edge.targetNode.routingData[criterionName]
						edgeData = edgeFunction( edge, targetData )
						edge.routingData[criterionName] = edgeData
						edgePriorities[edge] = edgeData.priority
					
					sortedEdges = sorted( node.edges,
						key = lambda e: edgePriorities[e] )
					
					if piece == lastPiece:
						# do not run node function again on second pass
						nodeData = node.routingData[criterionName]
					else:
						bestEdgeData = sortedEdges[0].routingData[criterionName]
						nodeData = nodeFunction( node, bestEdgeData )
						node.routingData[criterionName] = nodeData
						
					nodeData.sortedEdges = sortedEdges
					nodeData.bestEdge = sortedEdges[0]
			
			if piece == lastPiece and nextPiece != None: break
			nextPiece = piece
			piece = piece.previous
			
		self.routingCriterion = criterionName
		
	
	def routeByShortest( self ):
		"""
		Create routing by distance and number of switches along
		route. Adds key 'shortest' to routingData of nodes and edges
		"""
		
		def edgeFunction( edge, targetData ):
			
			distance = targetData.distance
			switches = targetData.switches + abs(edge.intDirection)
			
			return Struct({
				'distance': distance,
				'switches': switches,
				'priority': ( distance, switches ) })
		
		def nodeFunction( node, bestEdgeData ):
			
			if bestEdgeData == None:
				s = 0
				d = node.lane.length
			else:
				s = bestEdgeData.switches
				d = bestEdgeData.distance + node.lane.length
			
			return Struct({ 'distance': d,'switches' : s })
		
		self.routeBy( 'shortest', nodeFunction, edgeFunction )
	
	def routeByUnobstructed( self, beginPiece, blockedLanes ):
		"""
		Route avoiding obstacles
		"""
		
		def edgeFunction( edge, targetData ):
			
			freeDistance = targetData.freeDistance
			distance = targetData.distance
			switches = targetData.switches + abs(edge.intDirection)
			
			return Struct({
				'freeDistance': freeDistance,
				'distance': distance,
				'switches': switches,
				'priority': ( -freeDistance, distance, switches ) })
		
		def nodeFunction( node, bestEdgeData ):
			
			if bestEdgeData == None:
				freeDistance = 0
				s = node.routingData['shortest'].switches
				d = node.routingData['shortest'].distance
			else:
				freeDistance = bestEdgeData.freeDistance + 1
				s = bestEdgeData.switches
				d = bestEdgeData.distance + node.lane.length
			
			if node.lane in blockedLanes: freeDistance = 0
			
			return Struct({
				'freeDistance': freeDistance,
				'distance': d,
				'switches': s })
		
		self.routeBy( 'unobstructed', nodeFunction, edgeFunction, beginPiece )
	
	def buildGraph( self ):
		
		"""
		Initializes the switching graph of the track. The nodes,
		which represent the lanes are stored in laneGraph. Each
		of these nodes has edges to other nodes. The edges
		represent possible switches (including staying on the same
		lane)
		"""
		
		track = self.track
		
		self.laneGraph = {}
		
		# Loop through the whole track starting from finish with
		# an ovelap at the last track piece (see below)
		pieceIndex = len(track.pieces)-1
		nextPiece = None
		
		
		while pieceIndex >= -1:
			piece = track.pieces[pieceIndex]
			
			for laneIndex in range(len(track.lanes)):
				
				lane = piece.lanes[laneIndex]
			
				# Last track piece (second pass: build edges but do not
				# rewrite node object)
				if pieceIndex == -1:
					laneNode = self.laneGraph[lane]
				else:
					laneNode = Struct({})
					laneNode.routingData = {}
					laneNode.lane = lane
				
				self.laneGraph[lane] = laneNode
				
				# Last track piece (first pass) create node object
				# that is referenced by other nodes in the graph, but
				# do not build edges yet
				if nextPiece == None: continue
				
				switchDeltas = [0]
				if nextPiece.switch:
					if laneIndex > 0:
						switchDeltas.insert(0, -1)
					if laneIndex < len(track.lanes) - 1:
						switchDeltas.append(1)
				
				laneNode.edges = []
				for delta in switchDeltas:
					targetIndex = laneIndex + delta
					targetLane = nextPiece.lanes[targetIndex]
					targetNode = self.laneGraph[targetLane]
					
					laneNode.edges.append( self.__class__.Edge(
						targetNode, delta
					))
				
			nextPiece = piece
			pieceIndex -= 1
	
	def __init__(self, track):
		self.track = track
		self.buildGraph()
		self.routeByShortest()
	
	def buildRoute( self, beginLane, prevLane = None):
		return TrackGraph.Route( self, self.routingCriterion, beginLane, prevLane )
