
# Dummy opponents

from bot import *

# Overridden dummy traits

class NoSwitching:
	def handleSwitching( self ): return None

class ConstantThrottle:
	def __init__(self, throttle):
		self.constantThrottle = throttle
	
	def handleThrottle( self ): return ('throttle', self.constantThrottle)

class BrainDead:
	def updatePlan( self ): pass
	def handleSwitching( self ): return None
	def handleTurbo( self ): return None

class SafeThrottle:
	def handleThrottle( self ):
		if self.race.myCar.position.angle < 5.0: throttle = 0.5
		else: throttle = 0.1
		return ('throttle', throttle)
		
# And the dummies themselves


class SafeBot(BrainDead, SafeThrottle, Bot):
	"""Drives slowly and safely"""
	pass

class ObstacleBot(BrainDead, ConstantThrottle, Bot):
	"""Never throttles""" 
	
	def __init__(self, *args, **kwargs):
		ConstantThrottle.__init__(self, 0.0)
		Bot.__init__(self, *args, **kwargs)

class TrafficJamBot(BrainDead, ConstantThrottle, Bot):
	"""Moves slowly on one lane""" 
	
	def __init__(self, *args, **kwargs):
		ConstantThrottle.__init__(self, 0.2)
		Bot.__init__(self, *args, **kwargs)

class RoutedSafeBot(SafeThrottle, Bot):
	"""Safe throttle, shortest path routing""" 
	
	def handleTurbo( self ): return None
	
	def __init__(self, *args, **kwargs):
		Bot.__init__(self, *args, **kwargs)
	

MY_BOT = COMPETITION_BOT_CLASS

class MyBotWithoutTurbo(MY_BOT):
	def __init__(self, *args, **kwargs): MY_BOT.__init__(self, *args, **kwargs)
	
	def handleTurbo( self ):
		MY_BOT.handleTurbo( self )
		return None

class MyBotWithoutSwitching(NoSwitching, MY_BOT):
	def __init__(self, *args, **kwargs): MY_BOT.__init__(self, *args, **kwargs)

class SlowerMyBot(MY_BOT):
	def __init__(self, race, oldBot = None):
			MY_BOT.__init__(self, race, oldBot, 0.3)
			
class FasterMyBot(MY_BOT): # there's a catch :)
	def __init__(self, race, oldBot = None):
			MY_BOT.__init__(self, race, oldBot, -0.3)

DUMMY_BOT_CLASSES = [
	SafeBot,
	ObstacleBot,
	TrafficJamBot,
	RoutedSafeBot,
	MyBotWithoutTurbo,
	MyBotWithoutSwitching,
	SlowerMyBot,
	FasterMyBot ]
