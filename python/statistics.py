
import numpy
import time
from bot import Bot, Physics

class Statistics:
	"""
	Accumulates data about the game state for offline investigation
	"""
	
	def __init__(self, race):
		self.race = race
		self.matrix = None
		self.predictedAcceleration = None
		self.lastThrottle = None
	
	def assimilate(self):
		
		myCar = self.race.myCar
		
		special = (self.race.gameTick < 3
			or myCar.position == None
			or myCar.crashedAt != None)
		
		if not special:
			arr = [ self.race.gameTick,
					myCar.derivatives.ds,
					myCar.position.piece.index,
					myCar.position.lane.index,
					myCar.position.lane.length,
					myCar.position.inPieceDistance,
					myCar.throttle,
					myCar.position.angle,
					myCar.position.lane.signedCurvature ]
			arr = numpy.array(arr)
			
			if self.matrix == None: self.matrix = arr
			else: self.matrix = numpy.vstack((self.matrix,arr))
	
	def save(self):
		outfile = 'tmp/' + str(int(time.time())) + '.npy'
		numpy.save(outfile, self.matrix)
		print 'saved data to', outfile
