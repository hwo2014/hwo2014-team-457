
# Game data models

import math
from helpers import Struct, Vector2


class Car(Struct):
	"""
	The state of a single car. Is updated by receiving game
	messages. Some things about the state (such as throttle an turbo
	activation) are also set expicitly (not deduced from server
	messages)
	"""
	
	class Position(Struct):
		"""
		the parts of the state of a car, which can be resolved from a
		single carPositions message
		"""
		
		def __init__(self, car, carPositionData):
			Struct.__init__(self, carPositionData)
			
			self.inPieceDistance = self.piecePosition.inPieceDistance
			self.piece = car.track.pieces[self.piecePosition.pieceIndex]
			self.lane = self.piece.lanes[self.piecePosition.lane.endLaneIndex]
			self.distance = self.lane.beginDistance + self.inPieceDistance
			self.lap = self.piecePosition.lap
		
		@property
		def absolutePosition(self):
			return self.lane.absolutePosition( self.inPieceDistance )
		
		@property
		def absoluteAngle(self):
			return self.lane.absoluteAngle( self.inPieceDistance ) + self.angle
			
	
	class Derivatives(Struct):
		"""
		car state variables that depend on two (or more) consecutive
		gameTicks and must be computed
		"""
		
		def __init__( self ):
			Struct.__init__(self, {
				'velocity': 0.0,
				'forwardAcceleration': 0.0,
				'curveAcceleration': 0.0,
				'angularVelocity': 0.0,
				'angularAcceleration': 0.0,
				'absoluteAngularVelocity': 0.0,
				'absoluteAngularAcceleration': 0.0,
				'pieceChange': False,
				'laneChange': False,
				'lapChange': False,
				'prevLane': None,
				'prevPrevLane': None,
				'justInitialized': True
			})
			
		def update( self, oldPos, newPos, dt ):
			self.dt = dt
			self.justInitialized = False
			self.pieceChange = oldPos.piece != newPos.piece
			self.laneChange = oldPos.lane.index != newPos.lane.index
			self.lapChange = oldPos.lap != newPos.lap
			
			if self.pieceChange:
				switchLength = 0.0
				if self.prevLane != None and oldPos.lane in self.prevLane.switchLengths:
					switchLength = self.prevLane.switchLengths[oldPos.lane]
				
				self.prevPrevLane = self.prevLane
				self.prevLane = oldPos.lane
				self.ds = newPos.inPieceDistance + oldPos.lane.length - oldPos.inPieceDistance - switchLength
			else:
				self.ds = newPos.inPieceDistance - oldPos.inPieceDistance
				# do not change prevLane
			
			newVelocity = self.ds / float(dt)
			self.forwardAcceleration = (newVelocity - self.velocity) / float(dt)
			self.velocity = newVelocity
			self.curveAcceleration = self.velocity**2 * newPos.lane.signedCurvature
			newAngVel = (newPos.angle - oldPos.angle) / float(dt)
			self.angularAcceleration = newAngVel - self.angularVelocity
			self.angularVelocity = newAngVel
			newAbsoluteAngVel = (newPos.absoluteAngle - oldPos.absoluteAngle) / float(dt)
			self.absoluteAngularAcceleration = (newAbsoluteAngVel - self.absoluteAngularVelocity) / float(dt)
			self.absoluteAngularVelocity = newAbsoluteAngVel
			
	
	def __init__( self, track, carData ):
		Struct.__init__( self, carData )
		self.crashedAt = None
		self.dnf = None
		self.finish = None
		self.turboAvailable = None
		self.turbo = None
		self.throttle = 0.0
		self.derivatives = Car.Derivatives()
		self.position = None
		self.track = track
	
	def isOnTrack( self ):
		return (self.dnf == None and self.finish == None and
		        self.crashedAt == None and self.position != None)
	
	def currentSwitchLength( self ):
		if (self.derivatives.prevLane != None and
		    self.position != None and
		    self.position.lane in self.derivatives.prevLane.switchLengths):
			
			return self.derivatives.prevLane.switchLengths[self.position.lane]
		return 0.0
	
	def currentSwitchResolved( self ):
		return (self.derivatives.prevLane != None and
		    self.position != None and
		    (self.position.lane in self.derivatives.prevLane.switchLengths or
		     self.position.lane.index == self.derivatives.prevLane.index))
		
	
	@property
	def turboFactor( self ):
		if self.turbo == None: return 1.0
		else: return self.turbo.turboFactor
	
	def activateTurbo( self, currentGameTick ):
		self.turbo = self.turboAvailable
		self.turboAvailable = None
		self.turbo.activatedAt = currentGameTick + 1
		self.turbo.ticksLeft = self.turbo.turboDurationTicks + 3
	
	# The following methods update the car state based on server
	# messages
	
	def updatePosition( self, carPositionData, dt ):
		carPositionData.pop('id', None) # ignore id field
		newPosition = Car.Position(self, carPositionData)
		if self.position != None:
			self.derivatives.update( self.position, newPosition, dt )
		self.position = newPosition
		if self.turbo != None:
			self.turbo.ticksLeft -= dt
			if self.turbo.ticksLeft <= 0: self.turbo = None
	
	def receive_crash( self, crashMessage ):
		self.crashedAt = crashMessage['gameTick']
	
	def receive_spawn( self, spawnMessage ):
		self.crashedAt = None
		self.throttle = 0.0
		self.turboAvailable = None # Turbo lost on crash
	
	def receive_dnf( self, dnfMessage ):
		self.dnf = Struct(dnfMessage)
	
	def receive_finish( self, finishMessage ):
		self.finish = Struct(finishMessage)
	
	def receive_turboAvailable( self, turboAvailableMessage, currentGameTick = None ):
		self.turboAvailable = Struct(turboAvailableMessage)
		self.turboAvailable.receivedAt = currentGameTick


class Track(Struct):
	"""
	Models the race track. Consists of pieces (see Track.Piece) and
	lanes. The lanes of the Track object contain the global info
	received in the gameInit message (e.g, 3 lanes, distances from
	center -10, 0, 10)
	"""
	
	class Piece(Struct):
		"""
		A track piece. Can be either Bend or Straight. Each Piece
		contains as many Lanes as its parent Track object.
		"""
		
		class Lane(Struct):
			"""
			A Lane inside a Piece. Lanes have attributes such as
			beginPosition, length and (signed) curvature
			"""
			
			def __init__(self, trackLane, piece, previousPiece):
				Struct.__init__(self, trackLane.__dict__)
				self.piece = piece
				self.switchLengths = {}
				if previousPiece == None: self.beginDistance = 0.0
				else:
					# this lane in the previous piece
					self.previous = previousPiece.lanes[self.index]
					previousPiece.lanes[self.index].next = self
					self.beginDistance = self.previous.length + self.previous.beginDistance
			
			
			def __str__(self):
				return 'lane %d @ piece %d' % (self.index, self.piece.index)
			
			# (visualization) helpers for transforming local
			# inPieceDistances to global parameters (Vector2's and
			# angles)
					
			def absolutePosition( self, inPieceDistance ):
				return self.piece.absolutePosition( inPieceDistance,
					self.distanceFromCenter )
			
			def absoluteAngle( self, inPieceDistance ):
				return self.piece.absoluteAngle( inPieceDistance,
					self.distanceFromCenter )
			
		def __init__(self, track, data, previous):
			Struct.__init__( self, data )
			self.previous = previous
			if 'switch' not in data: self.switch = False
			if previous == None:
				self.begin = Struct({
					'position' : Vector2(0.0,0.0),
					'angle' : 0.0
				})
				self.index = 0
				self.first = True
			else:
				self.begin = previous.end
				self.index = previous.index + 1
			self.finish = False
			# somewhat ugly, also affects previous.end (which is ok)
			self.begin.direction = Vector2.fromAngle( self.begin.angle ).perpCw()
			
			self.lanes = [Track.Piece.Lane(l, self, previous) for l in track.lanes]
	
	class Straight(Piece):
		def __init__(self, track, data, previous):
			Track.Piece.__init__( self, track, data, previous )
			endPos = self.begin.position + self.begin.direction * self.length
			self.end = Struct({
				'position': endPos,
				'angle': self.begin.angle
				})
			for lane in self.lanes:
				lane.length = self.length
				lane.curvature = 0.0
				lane.signedCurvature = 0.0
		
		# Helpers for computing lane properties (see the helpers in
		# Lane for example use)
		
		def absoluteAngle( self, inPieceDistance, laneDelta ):
			return self.begin.angle
		
		def absolutePosition( self, inPieceDistance, laneDelta ):
			perp = self.begin.direction.perpCw()
			return self.begin.position + perp * laneDelta + self.begin.direction * inPieceDistance
			
	class Bend(Piece):
		def __init__(self, track, data, previous):
			Track.Piece.__init__( self, track, data, previous )
			self.sign = -1 if self.angle < 0 else 1
			turningCenterDir = self.begin.direction.perpCw() * self.sign
			relCenter = turningCenterDir * self.radius
			self.turningCenter = self.begin.position + relCenter
			endAngle = self.begin.angle + self.angle
			endPos = self.turningCenter + Vector2.fromAngle(endAngle) * self.radius * self.sign
			self.end = Struct({
				'position': endPos,
				'angle': endAngle
				})
			for lane in self.lanes:
				lane.radius = self.laneRadius( lane.distanceFromCenter )
				lane.length = self.laneLength( lane.distanceFromCenter )
				lane.curvature = 1.0 / lane.radius
				lane.signedCurvature = lane.curvature * self.sign
		
		# Helpers for computing lane properties (see the helpers in
		# Lane for example use)
		
		def laneRadius( self, laneDelta ):
			return self.radius - laneDelta * self.sign
		
		def laneLength( self, laneDelta ):
			return abs(self.angle) / 180.0 * math.pi * self.laneRadius(laneDelta)
		
		def absoluteAngle( self, inPieceDistance, laneDelta ):
			fracAngle = inPieceDistance / self.laneLength( laneDelta )
			return self.begin.angle + fracAngle * self.angle
			
		def absolutePosition( self, inPieceDistance, laneDelta ):
			r = self.laneRadius( laneDelta ) * self.sign
			finalAngle = self.absoluteAngle( inPieceDistance, laneDelta )
			return self.turningCenter + Vector2.fromAngle(finalAngle) * r
	
	def __init__( self, trackData ):
		pieces = trackData.pop('pieces')
		
		Struct.__init__( self, trackData )
		self.pieces = []
		prevPiece = None
		self.nSwitches = 0
		for pieceData in pieces:
			if 'radius' in pieceData: class_ = Track.Bend
			else: class_ = Track.Straight
			piece = class_(self, pieceData, prevPiece)
			self.pieces.append( piece )
			if prevPiece != None: prevPiece.next = piece
			prevPiece = piece
			if piece.switch:
				piece.switchIndex = self.nSwitches
				self.nSwitches += 1
		
		self.pieces[-1].finish = True
		self.pieces[0].previous = self.pieces[-1]
		self.pieces[-1].next = self.pieces[0]
		
		for lane in self.lanes:
			last = self.pieces[-1].lanes[lane.index]
			lane.length = last.beginDistance + last.length
			self.pieces[0].lanes[lane.index].previous = last
			last.next = self.pieces[0].lanes[lane.index]

class Race:
	"""
	Race is the full game state. It contains the track and cars
	(including myCar). The state of the race is updated using
	the receiveMessage function that eats a server message
	"""

	def __init__(self):
		self.started = False
		self.gameTick = None
		self.track = None
	
	def receiveMessage( self, message ):
		msgType = message['msgType']
		handler = getattr( self, '_receive_'+msgType, None )
		if handler == None: return False
		handler( message )
		return True
	
	# helpers for the bot
	
	def getOpponentsOnTrack( self, includeCrashed = False ):
		return [ car for car in self.cars.values()
		         if car != self.myCar and \
					(car.isOnTrack() or \
						(includeCrashed and car.crashedAt != None)) ]
	
	def getBlockedLanes( self, includeCrashed = False, includeAdjacent = False ):
		# all lanes with opponent cars that are on the track
		laneDict = { car.position.lane : True for car in self.getOpponentsOnTrack(includeCrashed) }
		if includeAdjacent:
			for lane in laneDict.keys():
				laneDict[lane.next] = True
				laneDict[lane.previous] = True
		return laneDict.keys()
	
	def isQualifying( self ):
		return self.raceSession.laps == None
	
	# receiveMessage delegates things to the following helpers
	
	def _receive_yourCar( self, yourCarMessage ):
		self.myCarColor = yourCarMessage['data']['color']
	
	def _receive_gameInit( self, gameInitMessage ):
		raceData = gameInitMessage['data']['race']
		
		# Re-using old track
		if self.track == None:
			self.track = Track(raceData['track'])
		
		# Store cars into a color -> Car object dictionary
		self.cars = { data['id']['color']: Car(self.track, data) for data in raceData['cars'] }
		self.myCar = self.cars[self.myCarColor]
		self.raceSession = Struct(raceData['raceSession'])
		if 'laps' not in raceData['raceSession']:
			# handle different gameInit in qualifying rounds
			self.raceSession.laps = None
	
	def _receive_gameStart( self, gameStartMessage ):
		self.started = True
		
	def _receive_gameEnd( self, gameStartMessage ):
		self.started = False
	
	def _receive_carPositions( self, carPositionsMessage ):
		
		if 'gameTick' not in carPositionsMessage:
			print('ignoring carPositions without gameTick')
			return
		
		newGameTick = carPositionsMessage['gameTick']
		if self.gameTick != None: dt = newGameTick - self.gameTick
		else: dt = None
		self.gameTick = newGameTick
		for carPositionData in carPositionsMessage['data']:
			carId = carPositionData['id']['color']
			self.cars[ carId ].updatePosition( carPositionData, dt )
	
	def _receive_crash( self, msg ): self._sendCarMessage( msg )
	def _receive_spawn( self, msg ): self._sendCarMessage( msg )
	def _receive_finish( self, msg ): self._sendCarMessage( msg )
	
	def _receive_dnf( self, msg ):
		self.cars[ msg['data']['car']['color'] ].receive_dnf( msg )
		
	def _receive_turboAvailable( self, msg ):
		self.myCar.receive_turboAvailable( msg['data'], self.gameTick )
	
	def _sendCarMessage( self, message ):
		car = self.cars[ message['data']['color'] ]
		getattr(car, 'receive_'+message['msgType'])( message )
	
