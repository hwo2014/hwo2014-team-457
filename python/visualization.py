
# OpenGL visualization showing the race and bot thoughts in real time

import pygame
import sys
import time
import math
import numpy
import os
from OpenGL.GL import *
from OpenGL.GLU import *
from models import *
from helpers import Vector2

ENABLE_GL_CALL_LIST = os.environ.get('___DISABLE_GL_CALL_LIST') == None

class Visualization:
	
	SCREEN_SIZE = (1024,768)
	
	def __init__( self, race, bot, stats ):
		self.race = race
		self.bot = bot
		self.stats = stats
		
		pygame.init()
		screen = pygame.display.set_mode(
			Visualization.SCREEN_SIZE,
			pygame.DOUBLEBUF|pygame.OPENGL)
		
		pygame.display.set_caption("HWO 2014 track "+race.track.name)

		glShadeModel(GL_FLAT)
		glClearColor(0.0, 0.0, 0.0, 0.0)
		glPointSize(2.0)

		glEnable(GL_NORMALIZE)
		glEnable(GL_DEPTH_TEST)
		glMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, (1.0, 1.0, 1.0, 1.0))
		
		self._resize(*Visualization.SCREEN_SIZE)
		
		self.minX = 0
		self.minY = 0
		self.maxX = 1
		self.maxY = 1
		
		if ENABLE_GL_CALL_LIST:
			self.glListTrack = 1
			glNewList( self.glListTrack, GL_COMPILE )
		
		self.renderTrack()
		
		if ENABLE_GL_CALL_LIST: glEndList()
	
	def render( self ):
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

		glLoadIdentity()
		glTranslate( 0.5, 0.5 * self.aspectRatio, 0.0 )
		sc = 0.9 / max( self.maxX - self.minX, self.maxY - self.minY )
		glScale(sc,sc,sc)
		glTranslate( -(self.maxX+self.minX)*0.5, -(self.maxY+self.minY)*0.5, 0.0 )
		
		if ENABLE_GL_CALL_LIST: glCallList(self.glListTrack)
		else: self.renderTrack()
		self.renderBotData()
		self.renderCars()
		
		pygame.display.flip()
	
	def _resize( self, w, h):
		self.aspectRatio = h/float(w)
	
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity()
		
		gluOrtho2D(0, 1.0, 0, self.aspectRatio)
		
		glMatrixMode(GL_MODELVIEW)
	
	def renderPiece( self, piece ):
		def glVertexStoreMinMax( p ):
			x,y = (p.x, p.y)
			glVertex( x, y, 0 )
			self.minX = min(x, self.minX)
			self.minY = min(y, self.minY)
			self.maxX = max(x, self.maxX)
			self.maxY = max(y, self.maxY)
		
		reso = 14 if isinstance( piece, Track.Bend ) else 2
		
		glBegin(GL_POINTS)
		if piece.switch: glColor( 1.0, 0.0, 0.0 )
		else: glColor( 0.3, 0.3, 0.3 )
		p = piece.absolutePosition( 0, 0 )
		glVertex( p.x, p.y, 0.003 )
		glVertexStoreMinMax( p )
		glEnd()
		
		glLineWidth(3.0)
		for lane in piece.lanes:
			glColor( 0.4, 0.4, 0.4 )
			glBegin(GL_LINES)
			for i in xrange(reso):
				t0 = i/float(reso)
				t1 = (i+1)/float(reso)
				for t in (t0,t1):
					p = lane.absolutePosition( t * lane.length )
					glVertexStoreMinMax( p )
			glEnd()
	
	def renderBotData( self ):
		
		if not hasattr( self.bot, 'currentPlan' ): return
		plan = self.bot.currentPlan
		
		if hasattr( plan, 'route' ):
		
			glLineWidth(1.0)
			glBegin(GL_LINE_STRIP)
			for laneData in plan.route.laneDataList:
				
				lane = laneData.lane
				
				if laneData.pathIndex == 0: glColor(0.7,0.7,0.7)
				else: glColor(0.0,0.6,0.7)
				
				reso = 8 if isinstance( lane.piece, Track.Bend ) else 2
				for s in numpy.linspace( 0, lane.length, reso ):
					p = lane.absolutePosition( s )
					glVertex( p.x, p.y, 0.05 )
				
			glEnd()
		
		
		if hasattr( plan, 'trajectory' ):
			
			trajectory = plan.trajectory
			dists = trajectory.normalizedDistance
			pointPathIndices = numpy.interp( dists,
				plan.laneEndPoints,
				plan.pathIndices )
			
			glLineWidth(1.0)
			glBegin(GL_LINES)
			for index in xrange(len(dists)):
				distance = dists[index]
				pathIndex = int(pointPathIndices[index]) # TODO...
				laneData = plan.route.laneDataList[pathIndex]
				lane = laneData.lane
				inPieceDistance = distance - laneData.pathDistance
				active = True #trajectory.activeLookAhead[index]
				
				if active:
					glColor( 0.5, 1.0, 1.0 / (1.0 + trajectory.slack[index] * 0.1) )
				else:
					glColor( 0.4, 0.4, 0.7 )
				
				p = lane.absolutePosition( inPieceDistance )
				glVertex( p.x, p.y, 0.05 )
				
				angle = lane.absoluteAngle( inPieceDistance )
				angle += trajectory.angle[index]
				
				arrowLen = trajectory.velocity[index] * 5
				
				angDir = Vector2.fromAngle( angle - 90 )
				p2 = p + angDir * arrowLen
				
				glVertex( p2.x, p2.y, 0.05 )
				
				
			glEnd()
		
		if hasattr( self.bot, 'crashesPerPiece' ):
			for piece in self.race.track.pieces:
				color = None
				
				if piece in self.bot.crashesPerPiece:
				
					color = (1.0 / self.bot.crashesPerPiece[piece], 0.4, 0.4)
				
				elif self.bot.isOnLongestStraight( piece ):
					color = (0.4, 0.4, 0.6)
				
				elif self.bot.physics.pieceAnomalyCounters.get(piece,0) > 0:
					color = (0.8,0.6,0.8)
				
				elif self.bot.pieceAnomalies.get(piece,0) > 0:
					color = (0.8,0.8,0.1)
				
				reso = 14 if isinstance( piece, Track.Bend ) else 2
				
				glLineWidth(3.0)
				for lane in piece.lanes:
					
					laneColor = color
					meas = self.bot.physics.laneTorqueMeasurements.get(lane, None)
					
					if meas == None:
						if laneColor == None: 
							if abs(lane.signedCurvature) in self.bot.physics.curvatureMap:
								laneColor = (0.4,0.5,0.4)
							else:
								laneColor = (0.4,0.4,0.5)
					else:
						if meas.anomalyCounter > 0:
							laneColor = (0.8,0.4,0.8)
						elif meas.slope == None:
							if laneColor == None: laneColor = (0.7,0.7,0.4)
					
					if laneColor != None:
					
						glColor( *laneColor )
						glBegin(GL_LINES)
						for i in xrange(reso):
							t0 = i/float(reso)
							t1 = (i+1)/float(reso)
							for t in (t0,t1):
								p = lane.absolutePosition( t * lane.length )
								glVertex( p.x, p.y, 0.02 )
						glEnd()
				glLineWidth(1.0)
			
	
	def renderTrack( self ):
		for piece in self.race.track.pieces:
			self.renderPiece( piece )
			
	def carColor( self, car ):
		if car.id.color == 'red': return (1.0, 0.0, 0.0)
		if car.id.color == 'green': return (0.0, 1.0, 0.0)
		if car.id.color == 'blue': return (0.0, 0.0, 1.0)
		if car.id.color == 'purple': return (1.0, 0.0, 1.0)
		if car.id.color == 'yellow': return (1.0, 1.0, 0.0)
		if car.id.color == 'orange': return (1.0, 0.5, 0.0)
		return (1.0, 1,0, 1.0)
	
	def renderCars( self ):
		for car in self.race.cars.values():
			if car.position == None: continue
			
			p = car.position.absolutePosition
			
			glPushMatrix()
			angle = car.position.absoluteAngle
			glColor( *self.carColor( car ) )
			z = 1
				
			glTranslate( p.x, p.y, z )
			glRotate( -angle, 0, 0, 1 )
			w = car.dimensions.width
			l = car.dimensions.length
			x0 = car.dimensions.guideFlagPosition
			
			itr = 1
			if car.turboAvailable != None or car.turboFactor != 1.0: itr += 1
			
			for j in xrange(itr):
				solid = car.isOnTrack() and j == 0
				if solid: glBegin(GL_QUADS)
				else: glBegin(GL_LINE_STRIP)
				
				if j == 1:
					if car.turboAvailable != None:
						glColor( 0.5, 0.5, 0.5 )
					if car.turboFactor != 1.0:
						glColor( 0.0, 1.0, 1.0 )
				
				z = 0.01 * (1+j)
				glVertex( x0 - l, w * 0.5, z )
				glVertex( x0 - l, -w * 0.5, z )
				glVertex( x0, -w * 0.5, z )
				glVertex( x0, w * 0.5, z )
				if not solid: glVertex( x0 - l, w * 0.5, z )
				glEnd()
			
			glColor(0.0,0.0,0.0)
			glBegin(GL_POINTS)
			glVertex( 0, 0, 0.1 )
			glEnd()
			glPopMatrix()




