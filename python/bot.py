
import math
import numpy

from helpers import LinearResponse, Struct

from models import *
from physics import *
from routing import *

class Bot:
	"""
	The Bot makes the decision about the taken game move based on the
	game state (Race object).
	"""
	
	def __init__(  self, race, oldBot = None, slowDown = 0.0 ):
		self.race = race
		self.switchPendingFor = None
		self.currentPlan = None
		self.reusingOldBot = False
		self.isCrashed = False
		self.crashesPerPiece = {}
		self.extraSlowDown = slowDown
		self.safeToSwitch = True
		
		if oldBot == None:
			self.physics = Physics()
		else:
			print "re-using old bot components"
			self.physics = oldBot.physics
			self.reusingOldBot = True
	
	def onGameInit( self ):
		"""
		should be called when the race has been initialized. 
		precomputes AI guide data for the track.
		
		Is called twice when there are qualifying rounds
		"""
		
		self.trackGraph = TrackGraph(self.race.track)
	
	def makeMove( self ):
		"""Decides the bot move, i.e., tuple (action, data) """
		
		# Decision making is divided to the following phases, whose
		# implementations can be changed individually to create a
		# wide variety of test opponents
		
		if self.physics.update( self.race ):
			print "updated physics:\n"+str(self.physics)
			self.onPhysicsUpdated()
		
		self.updatePlan()
		
		actions = [
			self.handleSpecial,
			self.handleSwitching,
			self.handleTurbo,
			self.handleThrottle ]
		
		for action in actions:
			response = action()
			if response != None: return response
		
		print 'warning: fell through the decision loop'
		return ('ping', None)
		
	# --- Default implementations of the decision phases
	
	
	def updatePlan( self ):
		"""if the current plan needs to be updated, do it"""
		
		myCar = self.race.myCar
		if not myCar.isOnTrack(): return False
		
		if self.needNewRoute():
			
			self.beforeNewRoute()
			
			self.currentPlan = Struct({})
			pos = myCar.position.lane
			prevLane = myCar.derivatives.prevLane
			self.currentPlan.route = self.trackGraph.buildRoute( pos, prevLane )
			
			self.afterNewRoute( myCar.currentSwitchResolved() )
	
	def handleSpecial( self ):
		"""handle special cases such as begin crashed"""
		
		if self.race.myCar.crashedAt == None:
			self.isCrashed = False
		else:
			self.switchPendingFor = None # TODO ?
			
			if not self.isCrashed:
				self.physics.onMyCrash()
				self.onCrash()
			self.isCrashed = True
			
			return ('ping', None)
		
		return None
	
	def handleSwitching( self ):
		
		myCar = self.race.myCar
		pos = myCar.position
		
		# switch may have failed. this allows to recover (also works
		# for succesfull lane switches)
		if pos.lane != self.switchPendingFor:
			self.switchPendingFor = None
		
		nextPiece = pos.lane.next.piece
		
		if (nextPiece.switch and
			# Do not attempt to switch if switch message already sent
			self.switchPendingFor == None and
			# Do not send switch on first gameTick (or lose one tick of 
			# full throttle)
			self.race.gameTick != 1):
				
			# See if the route plan contains an order to switch
			switchDir = self.currentPlan.route.switchAt( myCar.position.lane )
			
			if switchDir != None:
				
				if not self.safeToSwitch:
					print 'skipping dangerous switch'
					return None
				
				if myCar.position.lane in self.race.getBlockedLanes(True):
					if myCar.position.inPieceDistance*2 < myCar.position.lane.length:
						print 'dealying switch when on blocked lane'
						return None
				
				self.switchPendingFor = pos.lane
				return ('switchLane', switchDir)
				#return ('ping', None)
		
		return None
	
	# Default implementation: do not use turbo
	def handleTurbo( self ): return None
	
	# handleThrottle must be implemented in subclasses
	
	
	# --- decision helpers
	
	# Called whenever the route plan is changed (override in subclasses)
	def beforeNewRoute( self ): pass
	def afterNewRoute( self, prevResolved = False ): pass
	
	def onPhysicsUpdated( self ): pass
	def onCrash( self ): pass
	
	def needNewRoute( self ):
		plan = self.currentPlan
		if plan == None: return True
		if self.race.gameTick == 1: return True
		
		carPos = self.race.myCar.position
		if not plan.route.hasLane( carPos.lane ): return True
		
		# can't reroute when already switching
		if self.switchPendingFor != None: return False
		
		piecesLeft = plan.route.laneDataMap[carPos.lane].pathElementsLeft
		
		# new route if half of the track is missing 
		return piecesLeft <= len(self.race.track.pieces)/3
	
	def isMyLastLap( self ):
		myCar = self.race.myCar
		# raceSession.laps can be None
		return myCar.position.lap + 1 == self.race.raceSession.laps
	
	def isOnHomeStraight( self, piece ):
		while True:
			if not isinstance(piece, Track.Straight): return False
			piece = piece.next
			if piece.index == 0: break
		return True
	
	def isOnLongestStraight( self, piece ):
		"""
		Check if a track piece is on the longest remaining straight of
		the current lap
		"""
		if not isinstance(piece, Track.Straight): return False
		
		curLen = 0.0
		firstLen = 0.0
		first = True
		stopIndex = piece.previous.index
		while piece.index != stopIndex:
			if isinstance(piece, Track.Straight):
				curLen += piece.length
			else:
				if first:
					firstLen = curLen
					first = False
				else:
					if curLen > firstLen: return False
				curLen = 0.0
			piece = piece.next
		return True
		

class LocalControlBot(Bot):
	"""Attempts to drive at maximal possible throttle at this moment"""
	
	def __init__(self, race, oldBot = None, slowDown = 0.0):
		
		#slowDown += 1.30 # makes the bot initially slower
		
		Bot.__init__(self, race, oldBot, slowDown)
	
		self.nLookAhead = 100
		
		self.onPhysicsUpdated()
		
		self.predictedAngle = 0.0
		self.lastGameTick = 0
		self.visitedLanes = {}
		self.nCrashes = 0
		self.oldPlan = None
		self.pieceAnomalies = {}
	
	def onPhysicsUpdated( self ):
		
		self.responsesFor = {}
		expectedTurbos = [1.0, 3.0]
		if hasattr( self.race, 'myCar' ):
			expectedTurbos.append( self.race.myCar.turboFactor )
		
		for turbo in set(expectedTurbos):
			self.constructResponsesFor( turbo )
		
		if self.currentPlan != None:
			self.afterNewRoute(self.race.myCar.currentSwitchResolved())
	
	def onCrash( self ):
		"""called whenever my bot crashes"""
		
		myCar = self.race.myCar
		piece = myCar.position.piece
		print "CRASH!!! at piece", piece.index
		
		#if myCar.position.lane in self.race.getBlockedLanes():
		#	print "other cars on lane. refusing to learn anything"
		#else:
		
		self.nCrashes += 1
		if piece not in self.crashesPerPiece: self.crashesPerPiece[piece] = 0
		self.crashesPerPiece[piece] += 1
	
	def needNewRoute( self ):
		myCar = self.race.myCar
		# build new route every piece change
		
		if Bot.needNewRoute( self ): return True
		elif len( self.race.cars ) == 1: return False
		
		# can't reroute when already switching
		if self.switchPendingFor != None: return False
		
		lane = myCar.position.lane 
		piecesLeft = self.currentPlan.route.laneDataMap[lane].pathElementsLeft
		
		# new route if many pieces are missing (do this with lower
		# threshold than in the base class because this can still be
		# aborted if the new route is not safe)
		if piecesLeft <= len(self.race.track.pieces)/3*2:
			return True
		
		if any( [c.derivatives.pieceChange for c in self.race.cars.values()] ):
			
			pathHead = self.currentPlan.route.pathStartingFrom( lane )
			pathHead = pathHead[:10] # pieces
			pathHead = {p.lane: True for p in pathHead}
			
			for l in self.race.getBlockedLanes(True,True):
				if l in pathHead: return True
		
		return False
	
	def beforeNewRoute( self ):
		
		myCar = self.race.myCar
		includeCrashed = True # crashed cars are time bombs
		# also consider the lanes directly in front of cars as blocked
		includeNextLanes = True
		blockedLanes = self.race.getBlockedLanes(includeCrashed, includeNextLanes)
		self.oldPlan = self.currentPlan
		
		if len( self.race.cars ) > 1:
		
			print " --- routing by unobstructed, ", len(blockedLanes), "blocked lanes"
			self.trackGraph.routeByUnobstructed( myCar.position.piece, blockedLanes )
		else:
			print '--- shortest path routing'
			self.trackGraph.routingCriterion = 'shortest'
	
		
	def afterNewRoute( self, prevResolved = False ):
		
		#allMeasured = len(self.physics.measuredPieces) == len(self.race.track.pieces)
		
		if self.oldPlan == None:
			reallyNeedRoute = True
		else:
			newPlan = self.currentPlan
			self.currentPlan = self.oldPlan
			reallyNeedRoute = Bot.needNewRoute( self )
			self.currentPlan = newPlan
		
		# Build curvature and lane index grids for the current route
		
		laneEndPointGrid = []
		curvatureGrid = []
		pathIndexGrid = []
		crashGrid = []
		slackGrid = []
		torqueSlopeGrid = []
		pathIndexToLane = {}
		
		for laneData in self.currentPlan.route.laneDataList:
			
			lane = laneData.lane
			piece = lane.piece
			curv = lane.signedCurvature
			laneEndPointGrid.append(laneData.pathDistance)
			laneEndPointGrid.append(laneData.pathDistance + lane.length)
			
			curvatureGrid.append( curv )
			curvatureGrid.append( curv )
			
			pathIndexGrid.append( laneData.pathIndex )
			pathIndexGrid.append( laneData.pathIndex )
			
			pathIndexToLane[laneData.pathIndex] = lane
			
			nCrashesAt = self.crashesPerPiece.get(piece,0)
			
			crashGrid.append( nCrashesAt )
			crashGrid.append( nCrashesAt )
			
			slack = 0.0
			torqueSlope = 0.0
			
			slack += nCrashesAt * 12.0
			
			meas = self.physics.laneTorqueMeasurements.get(lane, None)
			
			if not self.physics.frictionAnomaly:
				if lane.piece not in self.physics.measuredPieces:
					slack += 5.0
				
				if self.physics.pieceAnomalyCounters.get(lane.piece,0) > 0:
					slack += 7.0
			
				if meas != None:
					if meas.slope != None:
						torqueSlope = meas.slope
					else:
						if meas.anomalyCounter > 0:
							pass
							#slack += 5.0
						else:
							slack += 4.0
				
				if self.pieceAnomalies.get(piece,0) > 0: slack += 5.0
				
				if curv != 0.0 and torqueSlope == 0.0 and (\
					  meas == None or meas.anomalyCounter == 0):
					if abs(curv) in self.physics.curvatureMap:
						torqueSlope = self.physics.curvatureMap[abs(curv)]
					else:
						slack += 3.0
			
			# handle switches
			resolved = self.currentPlan.route.switchResolvedAt( lane )
			
			if not resolved or not prevResolved:
				slack += 5.0
			elif self.currentPlan.route.hasSwitchAt( lane ):
				slack += abs(self.currentPlan.route.switchLengthAt( lane )) * 0.1
				slack += 0.2
			
			prevResolved = resolved
			
			torqueSlopeGrid.append( torqueSlope )
			torqueSlopeGrid.append( torqueSlope )
			
			slackGrid.append( slack )
			slackGrid.append( slack )
			
		self.currentPlan.laneEndPoints = numpy.array(laneEndPointGrid)
		self.currentPlan.signedCurvatures = numpy.array(curvatureGrid)
		self.currentPlan.pathIndices = numpy.array(pathIndexGrid)
		self.currentPlan.crashArray = numpy.array(crashGrid)
		self.currentPlan.slackArray = numpy.array(slackGrid)
		self.currentPlan.torqueSlopeArray = numpy.array(torqueSlopeGrid)
		self.currentPlan.pathIndexToLane = pathIndexToLane
		
		if not reallyNeedRoute:
			# check safety of new route
			
			responses = self.responsesFor[self.race.myCar.turboFactor]
			pattern = numpy.zeros((self.nLookAhead,))
			pattern[0] = self.race.myCar.throttle
			
			extraAngleSlack = 0.0
			
			newTrajectory = self.getTrajectory( responses, pattern )
			
			if self.isTrajectorySafe(newTrajectory, extraAngleSlack):
				print 'new route is a-OK'
			else:
				print "new route is too dangerous!!! not changing"
				self.currentPlan = self.oldPlan
	
	def constructResponsesFor( self, turboFactor ):
		# constructs linear and angular motion response functions
		# for a given turbo factor
		
		#print 'reconstructing responses for turbo', turboFactor
		
		alpha = 1.0 / (1.0 - self.physics.velocity_coefficient)
		beta = self.physics.throttle_coefficient * alpha
		
		k = Physics.GUESS_ANGLE_SPRING
		c = Physics.GUESS_ANGLE_DAMPING
		
		nLookAhead = self.nLookAhead
		
		if turboFactor != 1.0: beta *= turboFactor
		
		linear = LinearResponse(
			numpy.array( [
				[ 1,    1,        0    ],
				[ 0,    alpha,    beta ],
				[ 0,    0,        0    ] ] ),
			('distance', 'velocity', 'throttle'),
			nLookAhead )
		
		self.responsesFor[turboFactor] = Struct({'linear':linear})
	
	def resolveTrajectoryPoints( self, trajectory ):
		
		plan = self.currentPlan
		x = numpy.fmod( trajectory.distance, plan.route.totalPathDistance )
		c = numpy.interp( x, plan.laneEndPoints, plan.signedCurvatures )
		
		blockedLanes = self.race.getBlockedLanes(True,True)
		trajectory.blockedPathIndices = {}
		for idx,lane in plan.pathIndexToLane.iteritems():
			if lane in blockedLanes:
				trajectory.blockedPathIndices[idx] = True
		
		# TODO: this has a bug somehwere, disabling for safety...
		#if self.isMyLastLap():
		#	active[ trajectory.distance > plan.route.finishLength ] = False
		
		trajectory.normalizedDistance = x
		trajectory.signedCurvature = c
		trajectory.slack = numpy.interp( x, plan.laneEndPoints, plan.slackArray )
		trajectory.pathIndices = numpy.interp( x, plan.laneEndPoints, plan.pathIndices )
		trajectory.crashes = numpy.interp( x, plan.laneEndPoints, plan.crashArray )
		trajectory.torqueSlopes = numpy.interp( x, plan.laneEndPoints, plan.torqueSlopeArray )
	
	def torqueFunction( self, velocity, slope, curvature ):
		
		if curvature == 0.0: return 0.0
		if slope == 0.0:
			return self.physics.guessCurveTorqueFunction( curvature, velocity )
		else:
			return self.physics.resolvedTorqueFunction( slope, curvature, velocity )
	
	def predictNextAngle( self, trajectory ):
		
		linearState, angularState = self.getCarState()
		
		angle = angularState.angle
		angvel = angularState.angularVelocity
		
		v = linearState.velocity
		
		if len(trajectory.signedCurvature) < 2: return 0
			
		for i in xrange(2):
			
			slope = trajectory.torqueSlopes[i]
			if slope == 0.0: torque = 0.0
			else: torque = self.torqueFunction( v, slope, trajectory.signedCurvature[i] )
				
			aa = angvel * self.physics.angle_osc_angvel
			aa += angle * v * self.physics.angle_osc_angle_v
			aa += angvel * v * self.physics.angle_osc_angvel_v
			aa += torque
			
			angle += angvel
			angvel += aa
			
			v = trajectory.velocity[i]
		
		return angle
	
	
	def resolveTrajectory( self, trajectory, linearState, angularState ):
		
		self.resolveTrajectoryPoints( trajectory )
		
		trajectory.angle = numpy.zeros( trajectory.velocity.shape )
		trajectory.angularVelocity = numpy.zeros( trajectory.velocity.shape )
		trajectory.torque = numpy.zeros( trajectory.velocity.shape )
		
		angle = angularState.angle
		angvel = angularState.angularVelocity
		
		v = linearState.velocity
		curv0 = trajectory.signedCurvature[0]
		#pieceIndex = None
		pieceIndex = trajectory.pathIndices[0]
		totalSlack = 0.0
		
		#trajectory.slack[20:] += 0.1
		
		for i in xrange(len(trajectory.velocity)):
			
			curv = trajectory.signedCurvature[i]
			
			totalSlack += trajectory.slack[i] * 0.05
			if trajectory.pathIndices[i] != pieceIndex:
				pieceIndex = trajectory.pathIndices[i]
				if pieceIndex in trajectory.blockedPathIndices:
					totalSlack += 6.0
				totalSlack += trajectory.slack[i] * 0.5
			
			# accumulating angle slack
			trajectory.slack[i] = totalSlack
			
			#if curv != curv0:
			#	trajectory.slack[i] += 3.0
			
			torque = self.torqueFunction( v,
				trajectory.torqueSlopes[i], curv )
				
			aa = angvel * self.physics.angle_osc_angvel
			aa += angle * v * self.physics.angle_osc_angle_v
			aa += angvel * v * self.physics.angle_osc_angvel_v
			aa += torque
			
			angle += angvel
			trajectory.angle[i] = angle
			trajectory.torque[i] = torque
			
			angvel += aa
			trajectory.angularVelocity[i] = angvel
			
			v = trajectory.velocity[i]
	
	
	def handleTurbo( self ):
		"""
		Decide wether to activate turbo and do necessary computations
		for possible unknown turbo factors
		"""
		
		myCar = self.race.myCar
		if myCar.turboAvailable:
			factor = myCar.turboAvailable.turboFactor
			if factor not in self.responsesFor:
				constructResponsesFor( factor )
				
		# ... handle turbo and throttle in handleThrottle below
		return None
	
	def maxAngle( self, trajectory ):
		return numpy.max( numpy.abs(trajectory.angle) )
	
	def isTrajectorySafe( self, trajectory, extraSlack ):
		absangles = numpy.abs(trajectory.angle)
		slack = numpy.minimum( trajectory.slack + extraSlack, self.physics.max_angle - 5.0 )
		maxAngle = numpy.max( absangles + slack )
		
		return maxAngle < self.physics.max_angle
	
	def getTrajectory( self, responses, pattern ):
		
		carStateLin, carStateAng = self.getCarState()
		
		traj = responses.linear.responseToState( carStateLin )
		dResponse = responses.linear.distance.integrateResponseTo.throttle( pattern )
		vResponse = responses.linear.velocity.integrateResponseTo.throttle( pattern )
		
		traj.distance += dResponse
		traj.velocity += vResponse
		
		self.resolveTrajectory( traj, carStateLin, carStateAng )
		return traj
	
	def getNextCurvatureChange( self, trajectory, startingFrom = 0 ):
		curv = trajectory.signedCurvature[startingFrom]
		otherCurvs = trajectory.signedCurvature != curv
		otherCurvs[:startingFrom] = False
		change = numpy.nonzero(otherCurvs)[0]
		if len(change) > 0: return change[0]
		return None
	
	def getCarState( self ):
		
		myCar = self.race.myCar
		pos = myCar.position
		
		d0 = self.currentPlan.route.laneDataMap[myCar.position.lane].pathDistance
		s0 = d0 + myCar.position.inPieceDistance
		
		carStateLin = {
			'distance': s0 - myCar.derivatives.velocity,
			'velocity': myCar.derivatives.velocity,
			'throttle': 0.0
		}
			
		carStateAng = {
			'angle': myCar.position.angle - myCar.derivatives.angularVelocity,
			'angularVelocity': myCar.derivatives.angularVelocity,
			'torque': self.physics.laneTorqueFunction(
						myCar.position.lane, myCar.derivatives.velocity )
		}
		carStateAng = Struct(carStateAng)
		carStateLin = Struct(carStateLin)
		return (carStateLin, carStateAng)
	
	def handleThrottle( self ):
		
		carStateLin, carStateAng = self.getCarState()
		myCar = self.race.myCar
		curLane = myCar.position.lane
		
		self.visitedLanes[curLane] = True
		
		curv = curLane.signedCurvature
		plan = self.currentPlan
		dt = self.race.gameTick - self.lastGameTick
		
		otherCarsOnLane = myCar.position.lane in self.race.getBlockedLanes(True,True)
		
		extraAngleSlack = 0.0
		
		responses = self.responsesFor[myCar.turboFactor]
		vResponse = responses.linear.velocity.responseTo.throttle
		dResponse = responses.linear.distance.responseTo.throttle
		
		trajectory = Struct({})
		pattern = numpy.zeros( vResponse.shape )
		
		if myCar.derivatives.lapChange: print 'lap change'
		
		wasAngErr = False
		angErr = 0.0
		if dt == 1:
			angErr = abs(myCar.position.angle - self.predictedAngle)
			
			if hasattr(plan, 'trajectory'):
				zeroTrajectory = self.getTrajectory( responses, pattern )
				angErr = max(angErr, numpy.max( numpy.abs( plan.trajectory.angle[1:]-zeroTrajectory.angle[:-1] )))
			
			meas = self.physics.laneTorqueMeasurements.get(curLane,None)
			
			nAnom = self.pieceAnomalies.get(curLane.piece,0)
			if angErr > 1e-9:
				if not myCar.derivatives.pieceChange:
					if curv == 0.0 or abs(curv) in self.physics.curvatureMap or (meas != None and meas.anomalyCounter > 0):
						print 'angle anomaly', angErr, self.pieceAnomalies.get(curLane.piece,0)
						extraAngleSlack += 7.0
					else:
						print 'unresolved torque', angErr
						extraAngleSlack += 6.0
					
					if not otherCarsOnLane:
						incr = 1
						if len(self.physics.measuredPieces) == len(self.race.track.pieces):
							incr = 3
						self.pieceAnomalies[curLane.piece] = nAnom + incr
						
						if nAnom >= 10: self.physics.resetLaneTorque(curLane)
			else:
				if nAnom > 0: self.pieceAnomalies[curLane.piece] -= 1
				
		else:
			extraAngleSlack += 5.0
			print 'handleThrottle skip', dt
		
		if myCar.derivatives.pieceChange:
			extraAngleSlack += 5.0
		
		# driving safer after multiple crashes
		if self.nCrashes > 3:
			extraAngleSlack = (self.nCrashes-3) * 3.0
		
		extraAngleSlack += self.extraSlowDown * 6.0
		
		if otherCarsOnLane or curLane:
			# other cars on lane
			extraAngleSlack += 5.0
		
		if self.switchPendingFor != None:
			extraAngleSlack += 0.5
		
		switching = (myCar.derivatives.prevLane != None and
			myCar.derivatives.prevLane.index != myCar.position.lane.index)
				
		if switching:
			
			# doing lane changes
			if myCar.position.lane in myCar.derivatives.prevLane.switchLengths:
				extraAngleSlack += abs(myCar.derivatives.prevLane.switchLengths[myCar.position.lane]) + 0.1
				#extraAngleSlack += 1.5
			else:
				# unresolved switch
				extraAngleSlack += 7.0
				
		turboAngleSlack = extraAngleSlack + 7.0
		if myCar.turboFactor > 1.0: extraAngleSlack = turboAngleSlack
		
		if (myCar.turboAvailable and myCar.throttle == 1.0 and self.safeToSwitch):
			
			# check if turbo is ok at the moment
			turboResponses = self.responsesFor[myCar.turboAvailable.turboFactor]
			
			nTurbo = int(myCar.turboAvailable.turboDurationTicks)
			nTurbo = min(nTurbo, 14)
			
			turboPattern = numpy.zeros( vResponse.shape )
			turboPattern[1:nTurbo] = 1.0
			
			turboTrajectory = self.getTrajectory( turboResponses, turboPattern )
			
			if self.isTrajectorySafe(turboTrajectory, turboAngleSlack) and not switching:
				return ('turbo', 'turbo now!')
				
			if self.isOnLongestStraight( myCar.position.piece ):
				return ('turbo', 'longest straight turbo!')
			
			#if self.isOnHomeStraight( myCar.position.piece ):
			#	return ('turbo', 'home straight turbo!')
		
		minThrottle = 0.0
		
		# special case: getting stuck in bends at non-zero angle
		motivation = min(max(myCar.derivatives.velocity, abs(myCar.derivatives.angularVelocity)),1.0)
		if motivation < 1.0:
			minThrottle = (1.0-motivation)*0.05
			extraAngleSlack = min(extraAngleSlack, 0.5)
		
		nSteps = 10
		for throttle in numpy.linspace( 1.0, minThrottle, nSteps ):
			
			pattern[0] = throttle
			
			trajectory = self.getTrajectory( responses, pattern )
			trajectory.slack *= motivation
			if self.isTrajectorySafe(trajectory, extraAngleSlack): break
		
		plan.trajectory = trajectory
		
		pattern = numpy.zeros( vResponse.shape )
		pattern[0:2] = throttle
		switchTrajectory = self.getTrajectory( responses, pattern )
		self.safeToSwitch = self.isTrajectorySafe(switchTrajectory, extraAngleSlack)
		
		self.predictedAngle = self.predictNextAngle( trajectory )
		self.lastGameTick = self.race.gameTick
		
		print self.race.gameTick, myCar.turboFactor, motivation, "%.2g %.4g" % (angErr, myCar.derivatives.velocity),
		print myCar.position.angle, extraAngleSlack, self.maxAngle(trajectory), throttle
		
		return ('throttle', throttle)

# Which bot to use in the competition
COMPETITION_BOT_CLASS = LocalControlBot
