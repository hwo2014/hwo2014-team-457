
# Usage python test/stats.py tmp/TIMESTAMP.npy
#
# For offline investigation (plotting and curve fitting) of game data

import sys, math
import numpy
import matplotlib.pyplot as plt 


def shrink( boolArray, w = 3 ):
	new = numpy.zeros( boolArray.shape, dtype=bool )
	for i in range(len(boolArray)):
		if i >= w and i < len(boolArray)-w:
			new[i] = all( boolArray[i-3:i+3] )
	return new

def myLinFit( y, var ):
	
	M = numpy.concatenate( tuple([x[...,numpy.newaxis] for x in var]), axis = 1 )
	res = numpy.linalg.lstsq(M,y)
	sol = res[0]
	err = math.sqrt(sum(res[1]**2))
	
	return (sol, err)

class Data:
	def __init__( self, data, begin = None, nPoints = None ):
		
		self.columnNames = [
			't',
			'vel',
			'pieceIndex',
			'laneIndex',
			'laneLength',
			'inPieceDistance',
			'throttle',
			'angle',
			'curvature'
		]
		
		if begin != None: data = data[begin:]
		if nPoints != None: data = data[:nPoints]
		
		for i in range(len(self.columnNames)):
			setattr(self, self.columnNames[i], data[:,i])
		
		self.columnNames += [
			'acc',
			'angvel',
			'angacc',
			'dcurv',
			'sinang',
			'sinangvel',
			'sinangacc',
			'radang',
			'rolledVel',
			's',
			'pieceAndLane',
			'pieceAndLap'
		]
		
		self.acc = numpy.zeros( self.t.shape )
		self.angvel = numpy.zeros( self.t.shape )
		self.angacc = numpy.zeros( self.t.shape )
		self.dcurv = numpy.zeros( self.t.shape )
		
		self.pieceAndLane = self.pieceIndex * 10 + self.laneIndex
		
		dpiece = numpy.diff( self.pieceIndex )
		dpiece[dpiece != 0.0] = 1.0
		self.pieceAndLap = numpy.zeros( self.pieceIndex.shape )
		self.pieceAndLap[1:] = numpy.cumsum(dpiece)
		
		self.acc[1:] = numpy.diff( self.vel )
		self.angvel[:-1] = numpy.diff( self.angle )
		self.angacc[:-1] = numpy.diff( self.angvel )
		
		self.dcurv[1:] = numpy.diff( self.curvature )
		
		self.s = numpy.cumsum([0] + self.vel)
		
		self.radang = self.angle / 180.0 * math.pi
		
		self.sinang = numpy.sin(self.radang)
		self.sinangvel = numpy.zeros( self.t.shape )
		self.sinangacc = numpy.zeros( self.t.shape )
		self.sinangvel[:-1] = numpy.diff( self.sinang )
		self.sinangacc[:-1] = numpy.diff( self.sinangvel )
		
		self.rolledVel = numpy.roll(self.vel, -1 )
		self.curvature = numpy.roll(self.curvature, -1)
		
		self.selectRange( 2, -10 )
	
	def selectRange( self, begin, end ):
		self.selectByFunc( lambda x: x[begin:end] )
	
	def select( self, boolArray ):
		self.selectByFunc( lambda x: x[boolArray] )
	
	def selectByFunc( self, func ):
		for i in range(len(self.columnNames)):
			old = getattr(self, self.columnNames[i])
			new = func(old)
			setattr(self, self.columnNames[i], new)
			
	
	def solveThrottle( self ):
	
		N_FIRST = 40
		vel = self.vel[:N_FIRST]
		throttle = self.throttle[:N_FIRST]
		acc = self.acc[:N_FIRST]
		
		vel = vel
		throttle = throttle
		
		sol, err = myLinFit( acc, (vel, throttle) )
		
		if err > 1e-6:
			print 'residual', err
		else:
			"""
			fitted = numpy.dot(M, sol)
			plt.plot( var[0], y )
			plt.plot( var[0], fitted, 'rx' )
			plt.show()
			"""
			self.coeff_v = sol[0]
			self.coeff_t = sol[1]
			self.predAcc = self.coeff_v * self.vel + self.coeff_t * self.throttle
			self.columnNames.append('predAcc')
		
		#print 'throttling coeffs', 1.0 / sol[0], sol[1] / sol[0]
		print 'throttling coeffs', sol[0], sol[1]
		return sol	
		
	def solveTorqueFunction( self ):
		
		a = self.angle
		v = self.angvel
		
		free = v * -0.1 + a * self.rolledVel * -0.00125 + v * self.rolledVel * -0.00125
		
		TOL = 1e-9
		diff = self.angacc - free
		cacc = self.rolledVel**2 * self.curvature
		
		nonz = numpy.abs(diff) > TOL
		
		x = cacc[nonz]
		y = diff[nonz]
		vel = self.rolledVel[nonz]
		curvs = self.curvature[nonz]
		
		y[x < 0] = -y[x < 0]
		#vel[x < 0] = -vel[x < 0]
		curvs[x < 0] = -curvs[x < 0]
		x[x < 0] = -x[x < 0]
		

		
		y = y + 0.3 * vel #* numpy.sign(x)
		slopes = y/x
		
		
		#sl, ind = numpy.unique( slopes, return_index=True )
		#cu = curvs[ind]
		#print numpy.unique(slopes)
		
		cu, ind = numpy.unique( curvs, return_index=True )
		sl = slopes[ind]
		
		pivotCurv = 1/90.0
		if pivotCurv in cu:
			pivotSlope = sl[[c for c in cu].index(pivotCurv)]
		else:
			pivotSlope = 1.0
		
		
		print 'slope map'
		for i in xrange(len(sl)):
			print '\t', cu[i], '\t', sl[i], sl[i]/pivotSlope
			
			other = slopes[curvs == cu[i]]
			other = other[numpy.abs(other-sl[i]) > 1e-6]
			other = numpy.unique(other)
			if len(other) > 0: print other
			
			slopes[curvs == cu[i]] = sl[i]
		

		allSlopes = cacc * 0
		allSlopes[nonz] = slopes
		
		fitted = allSlopes * cacc - 0.3 * self.rolledVel * numpy.sign(cacc)
		#sol[0] * cacc + sol[1] * cacc * self.curvature + sol[2] * cacc * self.curvature**2 + sol[3]*self.rolledVel
		
		fitted[fitted * numpy.sign(cacc) <= 0] = 0.0
		
		
		print 'residual', sum(( diff - fitted )**2 )
		
		#"""
		plt.plot( cacc, diff, 'ro' )
		plt.plot( cacc, fitted, 'gx' )
		plt.show()
		#"""
		
		#print k,c
		return allSlopes
		
	
	def solveFreeAngleOscillation( self ):
		
		nocurve = self.curvature == 0.0
		
		sol, err = myLinFit( self.angacc[nocurve], [x[nocurve] \
			for x in(self.angvel, self.angle * self.rolledVel, self.angvel * self.rolledVel)] )
		
		print 'ang osc residual', err
		print 'ang osc params', sol
		return sol
	
	def solveTorque( self, freeOscillation ):
		
		torque = self.angacc - freeOscillation
		cacc = self.rolledVel**2 * self.curvature
		
		params = numpy.zeros( cacc.shape + (2,) )
		
		criterion = self.curvature
		
		for p in numpy.unique( criterion ):
			selected = criterion == p
			
			used = numpy.logical_and(numpy.abs(torque) > 1e-9, selected)
			torques = torque[used]
			caccs = cacc[used]
			vel = self.rolledVel[used]
			
			if len(torques) > 0:
				
				sol, err = myLinFit( torques, (caccs, vel) )
				
				print p, '=>', sol, 'residual', err
				
				params[selected,:] = sol
			#else:
			#	print 'no non-zero torques for', p
		
		pred = cacc * params[:,0] + self.rolledVel * params[:,1]
		pred[numpy.sign(pred) != numpy.sign(cacc)] = 0.0
		
		#"""
		plt.plot( cacc, torque, 'ro' )
		plt.plot( cacc, pred, 'gx' )
		plt.show()
		#"""
		
		plt.plot( self.t, torque, 'ro' )
		plt.plot( self.t, pred, 'gx' )
		
		#plt.plot( self.t, torque-pred )
		
		plt.plot( self.t, cacc )
		plt.plot( self.t, self.angle * 0.01 )
		plt.plot( self.t, self.angvel * 0.1 )
		
		#plt.plot( self.t, self.rolledVel * 0.1 )
		plt.plot( self.t, self.curvature * 30 )
		#plt.plot( self.t, self.acc )
		
		plt.show()
		
		return pred
		
	
	def solveAngAcc( self ):
		
		a = self.angle
		v = self.angvel
		
		aoCoeff = self.solveFreeAngleOscillation()
		
		freeOscillation = v * aoCoeff[0] + a * self.rolledVel * aoCoeff[1] + v * self.rolledVel * aoCoeff[2]
		
		print numpy.transpose(numpy.vstack( (self.t, self.rolledVel**2 * self.curvature, self.rolledVel, self.angacc - freeOscillation) ))
		
		torque = self.solveTorque( freeOscillation )
		
		pred = freeOscillation + torque
		
		y = self.angacc
		
		
		print 'err', sum((y-pred)**2)
		
		"""
		plt.plot( self.t, self.curvature * 100 )
		plt.plot( self.t, torque )
		plt.plot( self.t, y, 'ro' )
		plt.plot( self.t, pred, 'bx' )
		
		plt.show()
		"""



	
data = numpy.load(sys.argv[1])

x = Data(data)



#x.selectRange( 2011, 170 )
#x.selectRange( 2100, 3000 )

#print numpy.transpose(numpy.vstack( (x.t, x.curvature, x.rolledVel, x.angvel, x.angacc) ))

av,at = x.solveThrottle()
accAnomalies = abs(x.acc - x.predAcc) > 1e-6
x.select( shrink( numpy.logical_not(accAnomalies), 1 ) )

#x.select( x.curvature == 0.0 )
#x.selectRange( 50, 110 )

x.solveAngAcc()


