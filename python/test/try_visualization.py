
import json, time, sys, copy
sys.path.append('../')

from models import *
from bot import *
from dummies import *
from statistics import *
from visualization import Visualization

def loadFixture(name):
	return json.load( open('fixtures/'+name+'.json') )

race = Race()
bot = TrafficJamBot( race )
stats = Statistics( race )
race.receiveMessage( loadFixture('yourCar') )
#race.receiveMessage( loadFixture('gameInit') )
race.receiveMessage( loadFixture('gameInit.keimola') )
bot.onGameInit()
race.receiveMessage( loadFixture('gameStart') )

data0 = loadFixture('carPositions.red')

for i in xrange(1,10):
	data = copy.deepcopy(data0)
	data['gameTick'] = i
	race.receiveMessage( data )
	bot.makeMove()
	stats.assimilate()

visu = Visualization( race, bot, stats )
visu.render()
time.sleep(50)

