import unittest, sys, json, os, os.path, copy
sys.path.append('../')

from models import *
from bot import *
from statistics import *
from main import Game
from helpers import *
from dummies import *

class AssertionUtils:
	
	def assertAbout(self, a,b):
		eps = 1e-9
		self.assertTrue(abs(a-b) < eps)
	
	def assertVecsEqual(self, a,b):
		self.assertAbout(a.x, b.x)
		self.assertAbout(a.y, b.y)

class HelpersTest(unittest.TestCase, AssertionUtils):
	
	def test_struct( self ):
		s = Struct({
			'foo': 'bar',
			'list': [1,2,{ 'key': 'value' }],
			'nested': { 'key': 'value' }
			})
		
		self.assertEqual( s.foo, 'bar' )
		self.assertEqual( s.list[0], 1 )
		self.assertEqual( s.list[2].__class__, Struct )
		self.assertEqual( s.list[2].key, 'value' )
		self.assertEqual( s.nested.__class__, Struct )
		self.assertEqual( s.nested.key, 'value' )
		
		s._update( {'foo': 'baz', 'new': 'value2' } )
		self.assertEqual( s.foo, 'baz' )
		self.assertEqual( s.new, 'value2' )
	
	def test_vector( self ):
		
		v1 = Vector2( 1, 2 )
		v2 = Vector2( 3, 4 )
		
		self.assertVecsEqual( v1 + v2, Vector2( 4, 6 ) )
		self.assertVecsEqual( v1 * 2, Vector2( 2, 4 ) )
		self.assertVecsEqual( v1.perpCw(), Vector2( 2, -1 ) )
		self.assertVecsEqual( Vector2.fromAngle(0), Vector2(0,1) )
		self.assertVecsEqual( Vector2.fromAngle(90), Vector2(1,0) )
		self.assertVecsEqual( v1, Vector2(1,2) )
		self.assertVecsEqual( v2, Vector2(3,4) )
		

class Fixtures:
	def __init__(self):
		self._jsonFixtures = {}
		dirName = 'fixtures'
		for fn in os.listdir(dirName):
			if fn.endswith('.json'):
				data = json.load( open(dirName+'/'+fn) )
				self._jsonFixtures[fn.replace('.json','')] = data
	
	def __getitem__(self, what):
		return copy.deepcopy(self._jsonFixtures[what])

FIXTURES = Fixtures()


class ModelTests(unittest.TestCase, AssertionUtils):
	
	
	def test_car( self ):
		
		track = Track(FIXTURES['gameInit']['data']['race']['track'])
		
		car = Car(track, FIXTURES['gameInit']['data']['race']['cars'][0])
		self.assertEqual(car.id.name, 'Schumacher')
		self.assertEqual(car.dimensions.length, 40.0)
		self.assertEqual(car.crashedAt, None)
		self.assertEqual(car.dnf, None)
		self.assertEqual(car.finish, None)
		self.assertEqual(car.turboAvailable, None)
		self.assertEqual(car.throttle, 0.0)
		
		dt = 1
		for i in (1,2):
			carPositionFixture = FIXTURES['carPositions']['data'][0]
			car.updatePosition( carPositionFixture, dt )
			self.assertEqual(car.position.angle, 0.0)
			self.assertEqual(car.position.lane.index, 0)
			self.assertEqual(car.derivatives.velocity, 0.0)
			self.assertEqual(car.derivatives.forwardAcceleration, 0.0)
			self.assertEqual(car.derivatives.curveAcceleration, 0.0)
			
		newCarPos = FIXTURES['carPositions']['data'][0]
		newCarPos['piecePosition']['inPieceDistance'] = 0.3
		car.updatePosition( newCarPos, dt )
		self.assertEqual(car.derivatives.ds, 0.3)
		self.assertEqual(car.derivatives.velocity, 0.3)
		self.assertEqual(car.derivatives.forwardAcceleration, 0.3)
		self.assertEqual(car.derivatives.curveAcceleration, 0.0)
		self.assertFalse( car.derivatives.pieceChange )
		
		newCarPos = FIXTURES['carPositions']['data'][0]
		newCarPos['piecePosition']['pieceIndex'] = 2
		newCarPos['piecePosition']['inPieceDistance'] = 0.1
		car.updatePosition( newCarPos, dt )
		self.assertTrue( car.derivatives.pieceChange )
		self.assertFalse( car.derivatives.laneChange )
		# TODO...
		#self.assertEqual(car.derivatives.ds, 200 - 0.3 + 0.1)
		
		self.assertEqual( car.position.lap, 0 )
		
		newCarPos = FIXTURES['carPositions']['data'][0]
		newCarPos['piecePosition']['pieceIndex'] = 2
		newCarPos['piecePosition']['inPieceDistance'] = 0.2
		car.updatePosition( newCarPos, dt )
		self.assertEqual(car.derivatives.ds, 0.1)
		self.assertEqual(car.derivatives.velocity, 0.1)
		newCarPos = FIXTURES['carPositions']['data'][0]
		newCarPos['piecePosition']['pieceIndex'] = 2
		newCarPos['piecePosition']['inPieceDistance'] = 0.4
		car.updatePosition( newCarPos, dt )
		self.assertEqual(car.derivatives.ds, 0.2)
		self.assertEqual(car.derivatives.velocity, 0.2)
		self.assertEqual(car.derivatives.forwardAcceleration, 0.1)
		expected = 0.2*0.2 / car.position.lane.radius * car.position.piece.sign
		self.assertAbout(car.derivatives.curveAcceleration, expected)
		
		car.receive_turboAvailable( FIXTURES['turboAvailable']['data'] )
		self.assertEqual( car.turboAvailable.turboFactor, 3.0 )
		self.assertEqual( car.turbo, None )
		
		self.assertEqual( car.turboFactor, 1.0 )
		
		car.activateTurbo( 10 )
		self.assertEqual( car.turboAvailable, None )
		self.assertEqual( car.turbo.activatedAt, 10 + 1 ) # hack
		
		self.assertEqual( car.turboFactor, 3.0 )
		
		for i in xrange(30 + 3):
			self.assertEqual( car.turbo.ticksLeft, 30-i + 3 ) # worse hack
			car.updatePosition( FIXTURES['carPositions']['data'][0], 1 )
		
		self.assertEqual( car.turboAvailable, None )
		self.assertEqual( car.turbo, None )
		self.assertEqual( car.turboFactor, 1.0 )
		
	
	def test_track( self ):
		track = Track(FIXTURES['gameInit']['data']['race']['track'])
		self.assertEqual( len(track.lanes), 3 )
		self.assertEqual( len(track.pieces), 3 )
		self.assertTrue( isinstance( track.pieces[0], Track.Straight ) )
		self.assertTrue( isinstance( track.pieces[1], Track.Straight ) )
		self.assertTrue( isinstance( track.pieces[2], Track.Bend ) )
		
		p0,p1,p2 = track.pieces
		self.assertEqual( p0.index, 0 )
		self.assertEqual( p0.begin.angle, 0.0 )
		self.assertVecsEqual( p0.end.position, Vector2(100,0) )
		self.assertFalse( p0.switch )
		self.assertEqual( p1.index, 1 )
		self.assertVecsEqual( p1.begin.position, Vector2(100,0) )
		self.assertVecsEqual( p1.end.position, Vector2(200,0) )
		self.assertTrue( p1.switch )
		self.assertEqual( p2.angle, -22.5 )
		self.assertEqual( p2.begin.angle, 0.0 )
		self.assertEqual( p2.end.angle, -22.5 )
		self.assertVecsEqual( p2.turningCenter, Vector2(200,200) )
		self.assertFalse( p2.switch )
		
		self.assertVecsEqual( p0.lanes[0].absolutePosition( 0.0 ), Vector2( 0, 20 ) )
		self.assertVecsEqual( p1.lanes[0].absolutePosition( 0.0 ), Vector2( 100, 20 ) )
		self.assertEqual( p1.lanes[0].curvature, 0.0 )
		self.assertEqual( p0.lanes[0].length, 100 )
		self.assertEqual( p2.lanes[0].beginDistance, 200 )
		self.assertEqual( p2.lanes[2].beginDistance, 200 )
		self.assertEqual( p2.lanes[0].radius, 180 )
		self.assertEqual( p2.lanes[2].radius, 220 )
		self.assertAbout( p2.lanes[2].curvature, 1/220.0 )
		
		self.assertEqual( p0.lanes[0].previous, p2.lanes[0] )
		self.assertEqual( p1.lanes[1].previous, p0.lanes[1] )
		
		self.assertEqual( p0.lanes[0], p2.lanes[0].next )
		self.assertEqual( p1.lanes[1], p0.lanes[1].next )
		
		p2rad = abs(p2.angle) / 180.0 * math.pi
		p2len = p2rad * p2.lanes[0].radius
		self.assertAbout( p2.lanes[0].length, p2len )
		
		self.assertAbout( track.lanes[0].length, p2len + 200 )
		
	
	def test_race( self ):
		
		race = Race()
		
		race.receiveMessage( FIXTURES['yourCar'] )
		self.assertEqual( race.myCarColor, 'red' )
		
		race.receiveMessage( FIXTURES['gameInit'] )
		self.assertEqual( race.myCar.id.color, 'red' )
		self.assertEqual( race.raceSession.laps, 3 )
		
		race.receiveMessage( FIXTURES['gameStart'] )
		
		race.receiveMessage( FIXTURES['carPositions'] )
		self.assertEqual( race.gameTick, 0 )
		self.assertEqual( race.cars['blue'].position.angle, 45.0 )
		self.assertTrue( race.cars['blue'].isOnTrack() )
		
		race.receiveMessage( FIXTURES['crash'] )
		self.assertEqual( race.cars['blue'].crashedAt, 3 )
		self.assertFalse( race.cars['blue'].isOnTrack() )
		
		race.cars['blue'].throttle = 1.0
		race.receiveMessage( FIXTURES['spawn'] )
		self.assertEqual( race.cars['blue'].crashedAt, None )
		self.assertEqual( race.cars['blue'].throttle, 0.0 )
		self.assertTrue( race.cars['blue'].isOnTrack() )
		
		race.receiveMessage( FIXTURES['dnf'] )
		self.assertEqual( race.cars['blue'].dnf.data.reason, 'disconnected' )
		self.assertEqual( race.cars['blue'].dnf.gameTick, 650 )
		self.assertFalse( race.cars['blue'].isOnTrack() )
		
		self.assertTrue( race.cars['red'].isOnTrack() )
		race.receiveMessage( FIXTURES['finish'] )
		self.assertEqual( race.cars['red'].finish.gameTick, 2345 )
		self.assertFalse( race.cars['red'].isOnTrack() )
		self.assertFalse( race.cars['red'].isOnTrack() )
		
		race.receiveMessage( FIXTURES['turboAvailable'] )
		self.assertEqual( race.myCar.turboAvailable.turboFactor, 3.0 )

class TrackGraphTest(unittest.TestCase):
	
	def test_track_graph( self ):
		
		tracks = ['keimola', 'keimola.ci', 'france', 'usa', 'germany']
		for track in tracks:
			
			gameInit = FIXTURES['gameInit.'+track]
			track = Track(gameInit['data']['race']['track'])
			graph = TrackGraph(track)
			
			for piece in track.pieces:
				for lane in piece.lanes:
					#print piece.index, lane.index
					node = graph.laneGraph[lane]
					self.assertTrue( 'shortest' in node.routingData )
					for edge in node.edges:
						self.assertTrue('shortest' in edge.routingData)
					#print node.routingData['shortest']
					self.assertTrue(node.routingData['shortest'].bestEdge.targetNode.lane in graph.laneGraph)
			
					# should be able to build route starting from
					# any track piece and lane (and should not take
					# so long that this is would be infeasibl to unit
					# test :) )
					route = graph.buildRoute( lane )

class GameTests(unittest.TestCase):
	
	NAME = 'testbotname'
	KEY = 'secret'
	
	BOT_CLASSES = [COMPETITION_BOT_CLASS] + DUMMY_BOT_CLASSES
	
	def test_game_with_qualifying_rounds( self ):
		
		for BotClass in GameTests.BOT_CLASSES:
		
			game = Game( GameTests.NAME, GameTests.KEY, BotClass )
			
			
			joinMsg = game.joinMessage()
			
			self.assertEqual( None, game.receiveMessage( FIXTURES['yourCar'] ) )
			self.assertEqual( None, game.receiveMessage( FIXTURES['gameInit.keimola.qualifying'] ) )
			self.assertEqual( ('throttle',1.0), game.receiveMessage( FIXTURES['gameStart'] ) )
			
			# test some bot helpers
			bot = game.bot
			self.assertTrue( bot.isOnHomeStraight( game.race.track.pieces[-1] ) )
			self.assertFalse( bot.isOnHomeStraight( game.race.track.pieces[0] ) )
			self.assertTrue( bot.isOnLongestStraight( game.race.track.pieces[-5] ) )
			self.assertFalse( bot.isOnLongestStraight( game.race.track.pieces[10] ) )
			
			hadThrottle = False
			for i in xrange(1,10):
				data = FIXTURES['carPositions.red']
				data['gameTick'] = i
				response = game.receiveMessage( data )
				if response[0] == 'throttle': hadThrottle = True
				
			self.assertTrue( hadThrottle )
			
			self.assertEqual( None, game.receiveMessage( FIXTURES['gameEnd'] ) )
			
			self.assertEqual( None, game.receiveMessage( FIXTURES['gameInit.keimola'] ) )
			self.assertEqual( ('throttle',1.0), game.receiveMessage( FIXTURES['gameStart'] ) )
			
			hadThrottle = False
			for i in xrange(1,10):
				data = FIXTURES['carPositions.red']
				data['gameTick'] = i
				response = game.receiveMessage( data )
				if response[0] == 'throttle': hadThrottle = True
				
			self.assertTrue( hadThrottle )
			
			self.assertEqual( None, game.receiveMessage( FIXTURES['gameEnd'] ) )
			self.assertEqual( None, game.receiveMessage( FIXTURES['tournamentEnd'] ) )
	
	def test_game_withoug_qualifying_rounds( self ):
		
		tracks = ['keimola', 'keimola.ci', 'france', 'usa', 'germany']
		
		for BotClass in GameTests.BOT_CLASSES:
			
			for track in tracks:
				
				gameInitName = 'gameInit.'+track
			
				game = Game( GameTests.NAME, GameTests.KEY )
				
				joinMsg = game.joinMessage()
				
				self.assertEqual( None,  game.receiveMessage( FIXTURES['yourCar'] ) )
				self.assertEqual( None, game.receiveMessage( FIXTURES[gameInitName] ) )
				self.assertEqual( ('throttle',1.0), game.receiveMessage( FIXTURES['gameStart'] ) )
				
				hadThrottle = False
				for i in xrange(1,23):
					data = FIXTURES['carPositions.red']
					data['gameTick'] = i
					data['data'][0]['piecePosition']['inPieceDistance'] = (i % 8)**2 * 0.1
					data['data'][0]['piecePosition']['pieceIndex'] = i / 8
					response = game.receiveMessage( data )
					if response[0] == 'throttle': hadThrottle = True
					
					if i == 5:
						game.receiveMessage( FIXTURES['crash.red'] )
					
					if i == 9:
						game.receiveMessage( FIXTURES['spawn.red'] )
					
					if i == 12:
						game.receiveMessage( FIXTURES['turboAvailable'] )
					
				self.assertTrue( hadThrottle )
				
				self.assertEqual( None, game.receiveMessage( FIXTURES['gameEnd'] ) )
				self.assertEqual( None, game.receiveMessage( FIXTURES['tournamentEnd'] ) )
		

if __name__ == '__main__':
	unittest.main()
